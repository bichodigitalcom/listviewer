# DAILYTASK LOG EXPORT TO CSV

## Explicación

Toma el archivo de "../data/dailytasks-log.json" (log de dailytasks) y lo convierte a un CSV.

Reemplaza los "v" de hecho a "1" porque funciona mejor con la planilla de cálculos.

Pone los datos "extras", que son los datos luego del símbolo ">" en su propia columna, cuyo titulo es igual a la tarea correspondiente, más un "_xval".

## Uso

*Hecho con node 10.13.0 pero debería andar con cualquiera (es casi todo javascript puro)*

**node dailytasks_export.js > result.csv**