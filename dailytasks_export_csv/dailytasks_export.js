
var fs = require("fs");

let data = JSON.parse(fs.readFileSync("../data/dailytasks-log.json"));

/* DATA REFERENCE
"years": {
		"y2021": {
			"m09": {
				"d25": {
					v-taskname1
					-taskname2
					v-taskname3 > extra value
				}
			}
		}
}
*/

/** Dailytasks export version 2, print everything to csv
*/

let dayObjCurrent;
let dayObjPrevious;
let dayObjCurrentAndPreviousAreEqual = false;
let dayRow;
let iDOCArr;
for(var y in data.years){
	for(var m in data.years[y]){
		for(var d in data.years[y][m]){
			//prepare dayObj
			dayObjCurrent = data.years[y][m][d].tasks;
			dayObjCurrent = dayObjCurrent.map(iDOC=>{
				iDOCArr = iDOC.split("-"); //split done value from task name and return it as an array of 2 values
				if(iDOCArr.length>1 && iDOCArr[1].indexOf(">")!=-1){ //if the task name has an extra value, we add it as a third array value
					iDOCArr=iDOCArr[1].split(">").map(iDOCArr1=>iDOCArr1.trim()); //first we separate the task name from the extra value and set it as the array to return
					iDOCArr.unshift(iDOC.split("-")[0]); //then we add the done value as the first of the three array value
				}
				return iDOCArr;
			});
			//compare dayObj to previous day
			if(dayObjPrevious && dayObjPrevious.length==dayObjCurrent.length){
				dayObjCurrentAndPreviousAreEqual = true;
				for(var iDOP in dayObjPrevious){
					if(dayObjCurrent.filter(iDOC => iDOC[1] == dayObjPrevious[iDOP][1]).length>0){ //check if both day obj have the same tasks names
						continue
					} else {
						dayObjCurrentAndPreviousAreEqual = false;
						break;
					};
				}
			} else {
				dayObjCurrentAndPreviousAreEqual = false
			}
			
			if(!dayObjCurrentAndPreviousAreEqual){
				console.log(([""].concat(dayObjCurrent.map(iDOC=>"").concat(dayObjCurrent.map(iDOC=>"")))).toString()); //print empty row (for clear separation, visually)
				console.log((["fecha"].concat(dayObjCurrent.map(iDOC=>iDOC[1]).concat(dayObjCurrent.map(iDOC=>iDOC[1]+"_xval")))).toString()); //print row titles
			}
			console.log(([y.substr(1)+"/"+m.substr(1)+"/"+d.substr(1)].concat(dayObjCurrent.map(iDOC=>iDOC[0]=="v"?"1":iDOC[0]).concat(dayObjCurrent.map(iDOC=>iDOC[2]?'"'+iDOC[2]+'"':"")))).toString()); //print rows (values)
			
			//save current day as previous for the next iteration
			dayObjPrevious = dayObjCurrent;
		}
	}
}

/** Dailytasks export version 1, processing the data to print just one specific value I needed
*/
/*
let pesoval;
let dayval;
for(var y in data.years){
	for(var m in data.years[y]){
		for(var d in data.years[y][m]){
			dayval = data.years[y][m][d].tasks.filter(i=>i.search("pesar")>=0);
			if(dayval.length>0){
				dayval = dayval[0].split(">");
				if(dayval.length>1){
					pesoval=parseFloat(dayval[1].split(",").join("."));
				} else {
					continue;
				}
			} else {
				continue;
			}
			console.log(y.substr(1)+"/"+m.substr(1)+"/"+d.substr(1)+","+pesoval);
		}
	}
}*/