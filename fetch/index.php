<?php
?>
<html>
	<head>
	    <title>Fetch from bichodigital.com</title>
	    <meta charset="UTF-8">
	    <link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
	<h1>Fetch from Bichodigital.com</h1>
	<label for="output">Output:</label>
	<div id="output">Started</div>
	<div id="main">
		<div id="source" class="segment">
			<h2>source</h2>
			<label for="filepath">File path (from codiad's workspace)</label>
			<textarea id="filepath">test_codiad/tempmobile_codiad.txt</textarea>
		</div>
		<div id="destination" class="segment">
			<h2>destination</h2>
			<label for="destination-path">File path (from codiad's workspace)</label>
			<textarea id="destination-path">test_codiad/tempmobile_codiad.txt</textarea>
		</div>
	</div>
	<button id="btn-fetch" onclick="doFetch()">Fetch File</button>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script>
        function getURLParameter(name,url) {
            if(!url) url=location.search;
            else url="?"+url.split(/\?(.+)?/)[1]; //regexp that only matches the first "?" (had to scape it because it's a special character)
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || [null, ''])[1].replace(/\+/g, '%20')) || null;
        }
		function doFetch(){
			$("#output").html("fetching...");
			var postVars = {
				filepath:$("#filepath").val(),
				savepath:$("#destination-path").val()
			}
			$.post("fetch.php",postVars,function(response){
				if(response.status == "ok"){
					var date = new Date();
					var myDate = date.toTimeString().substring(0,date.toTimeString().indexOf("GMT"));
					$("#output").html("file fetched successfully - "+myDate);
				} else {
					$("#output").html("error fetching file");
				}
				
			},"json").error(function(e){
                $("#output").html("network or server error");
            });
		}
		$(document).ready(function(){
            var sourceFilepath = getURLParameter("filepath");
            if(!sourceFilepath){
                if(localStorage.getItem("fetchFromBichoDigitalSourceField"))
                    $("#filepath").val(localStorage.getItem("fetchFromBichoDigitalSourceField"));
                if(localStorage.getItem("fetchFromBichoDigitalDestField"))
                    $("#destination-path").val(localStorage.getItem("fetchFromBichoDigitalDestField"));
            } else {
                $("#filepath").val("test_codiad/"+sourceFilepath);
                $("#destination-path").val("test_codiad/"+sourceFilepath);
            }

			$("#filepath").bind("input propertychange",function(){
                localStorage.setItem("fetchFromBichoDigitalSourceField",$("#filepath").val());
                $("#destination-path").val($("#filepath").val());
                localStorage.setItem("fetchFromBichoDigitalDestField",$("#destination-path").val());
			})
            $("#destination-path").bind("input propertychange",function(){
                localStorage.setItem("fetchFromBichoDigitalDestField",$("#destination-path").val());
            })
		});
	</script>
	</body>
</html>