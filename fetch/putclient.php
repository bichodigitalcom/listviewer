<?php
?>
<html>
	<head>
	    <title>Put into bichodigital.com</title>
	    <meta charset="UTF-8">
	    <link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
	<h1>Put into Bichodigital.com</h1>
	<label for="output">Output:</label>
	<div id="output">Started</div>
	<div id="main">
		<div id="source" class="segment">
			<h2>source</h2>
			<label for="filepath">File path (from codiad's workspace)</label>
			<textarea id="filepath">test_codiad/tempmobile_codiad.txt</textarea>
		</div>
		<div id="destination" class="segment">
			<h2>destination</h2>
			<label for="destination-path">File path (from codiad's workspace)</label>
			<textarea id="destination-path">test_codiad/tempmobile_codiad.txt</textarea>
		</div>
	</div>
	<button id="btn-fetch" onclick="doPut()">Upload file</button>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script>
        function getURLParameter(name,url) {
            if(!url) url=location.search;
            else url="?"+url.split(/\?(.+)?/)[1]; //regexp that only matches the first "?" (had to scape it because it's a special character)
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || [null, ''])[1].replace(/\+/g, '%20')) || null;
        }
		function doPut(){
			$("#output").html("uploading...");
			$.get("../../"+$("#filepath").val(),function(data){
				var postVars = {
					filepath:$("#filepath").val(),
					contents:data,
					savepath:$("#destination-path").val()
				}
				$.post("http://www.bichodigital.com/codiad/workspace/test_codiad/fetch/putws.php",postVars,function(response){
					if(response.status == "ok"){
						var date = new Date();
						var myDate = date.toTimeString().substring(0,date.toTimeString().indexOf("GMT"));
						$("#output").html("file uploaded successfully - "+myDate);
					} else {
						$("#output").html("error uploading file");
					}
					
				},"json").error(function(e){
	                $("#output").html("network or server error");
	            });
			}).error(function(e){
				$("#output").html("file not found or localhost service not running (maybe binary file?)");
			});
			
		}
		$(document).ready(function(){
            var sourceFilepath = getURLParameter("filepath");
            console.log("sourcefilepath:"+sourceFilepath);
            if(!sourceFilepath){
                if(localStorage.getItem("putIntoBichoDigitalSourceField"))
                    $("#filepath").val(localStorage.getItem("putIntoBichoDigitalSourceField"));
                if(localStorage.getItem("putIntoBichoDigitalDestField"))
                    $("#destination-path").val(localStorage.getItem("putIntoBichoDigitalDestField"));
            } else {
                $("#filepath").val("test_codiad/"+sourceFilepath);
                $("#destination-path").val("test_codiad/"+sourceFilepath);
            }

			$("#filepath").bind("input propertychange",function(){
                localStorage.setItem("putIntoBichoDigitalSourceField",$("#filepath").val());
                $("#destination-path").val($("#filepath").val());
                localStorage.setItem("putIntoBichoDigitalDestField",$("#destination-path").val());
			})
            $("#destination-path").bind("input propertychange",function(){
                localStorage.setItem("putIntoBichoDigitalDestField",$("#destination-path").val());
            })
		});
	</script>
	</body>
</html>