<?php
	$filepath = $_REQUEST["filepath"];
	$savepath = $_REQUEST["savepath"];
	$contents = file_get_contents("http://www.bichodigital.com/codiad/workspace/".$filepath);

	$resultObj = new stdClass();

	if(!file_exists("../../".$savepath)){
		touch("../../".$savepath);
	} else {
		/** backup existing file*/
		$savefilename = str_ireplace("/","_",$savepath);
		$savefilename = substr($savefilename,0,strlen($savefilename)-4);
		$copyresult = copy("../../".$savepath,"/mnt/sdcard/external_sd/bk_test_codiad/fetchbk_".$savefilename.time().".txt");
		$resultObj->copyresult = $copyresult;
		$resultObj->filename = $savefilename;
	}
	$file = fopen("../../".$savepath,"w+");
	$saveresult = fwrite($file,$contents);
	fclose($file);
	
	/** print result */
	if(!$saveresult === false){
		$resultObj->status = "ok";
	} else {
		$resultObj->status = "error";
		$resultObj->error = "could not save the file fetched";
		$resultObj->data = "http://www.bichodigital.com/codiad/workspace/".$filepath;
	}
	echo json_encode($resultObj);
	