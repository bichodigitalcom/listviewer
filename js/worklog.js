/**
 * Worklog version 1.1
 * Objective: To track your own time at work.
 * Premise: I can work 8.5 hours a day as long as I stick to it. I want to know how they're tracking me and how well am I doing.
 *
 * Dependencies:
 * -requires momentjs library
 * -requires jQuery library
 *
 * */
 
 /*Polyfill for ES6 browsers */
 if (!Object.entries) {
  Object.entries = function( obj ){
    var ownProps = Object.keys( obj ),
        i = ownProps.length,
        resArray = new Array(i); // prealloacate the Array
    while (i--)
      resArray[i] = [ownProps[i], obj[ownProps[i]]];
    
    return resArray;
  };
}

/**/

var worklog = {};

worklog.stats = null;
worklog.log = null;
worklog.targetHours = 8.5;

/**
 * LOGIC
 *
 * */

worklog.loadLog = function(callback){
    $.post("data/worklog.json",null,function(response){
        worklog.log = response;
        callback();
    },'json');
}

worklog.saveLog = function(log){
    var postvars = {
        action: "save",
        body: JSON.stringify(log,null,"\t"),
        name: "worklog.json"
    }
    $.post("data/filemanager.php",postvars,function(response){
        return "ok";
    });
}

worklog.CHECK_TYPE_IN = "checkin";
worklog.CHECK_TYPE_OUT = "checkout";
worklog.check = function(timeStr,dateStr,checkType){
    var log = worklog.log;
    if(dateStr){
    	worklog.createSlot(log,dateStr); //para log manual
    } else {
    	worklog.createSlot(log);
    }
    var checkLog = {};
    var m = null
    if(timeStr && dateStr){
    	m = moment(dateStr+" "+timeStr); //TODO: make it parse the time and date string
    } else {
    	m = moment();
    }
    checkLog.time = m.format("H:mm");
    checkLog.date = m.format("YYYY-MM-DD");
    checkLog.unix = m.unix();
    //var timeofday = m.format("H");
    /*if(parseInt(timeofday)>14){
     checkLog.tag = "checkin";
     } else {
     checkLog.tag = "checkout";
     }*/
    var dayLogs = worklog.getDayLogs(log);
    if(!dayLogs.checkin || checkType == worklog.CHECK_TYPE_IN){
        /** log checkin */
        log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")].checkin = checkLog;
    } else {
        /** checkin assumed, logging checkout at the end */
        log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")].checkout = checkLog;
    }
    worklog.saveLog(log);
}

worklog.justify = function(hours,reason){
    var m = moment();
    worklog.createSlot(log);
    log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")].justified = {
        hours: hours,
        reason: reason
    }
    worklog.saveLog(log);
}

worklog.makeStats = function(){
    var log = worklog.log;
    var stats = {
        days:{},
        daysCheckins:{},
        text:"",
        html:"",
        history:{
            text:"",
            html:""
        }
    };
    var totalTrackedDays = 0;
    var totalHours = 0;
    if(!log.years) return;
    var lastDaysValue = parseInt($("#worklog-days").val());
    var daysToProcess = worklog.getLastNDays(lastDaysValue); //todo: make buttons for different ranges to know, and an offset of days ago
    for(var iDTP = 0; iDTP<daysToProcess.length;iDTP++){
        try{
            var checkin = daysToProcess[iDTP].checkin.time;
            var checkout = daysToProcess[iDTP].checkout.time;
            var justifiedHours = 0;
            if(daysToProcess[iDTP].justified){
                justifiedHours = daysToProcess[iDTP].justified.hours;
            }
            var ciArrTime = checkin.split(":");
            var coArrTime = checkout.split(":");
            var ciMins = parseInt(ciArrTime[0])*60+parseInt(ciArrTime[1]);
            var coMins = parseInt(coArrTime[0])*60+parseInt(coArrTime[1]);
            var hoursWorked = (coMins-ciMins)/60;
            hoursWorked+=justifiedHours;
            var hoursWorkedRounded = worklog.roundToTwoDecimals(hoursWorked);
            stats.history.text=daysToProcess[iDTP].checkin.date+": "+worklog.transcribeTime(hoursWorkedRounded)+stats.history.text;
            stats.history.html="<li>"+daysToProcess[iDTP].checkin.date+": "+worklog.transcribeTime(hoursWorkedRounded)+"</li>"+stats.history.html;
            stats.days[daysToProcess[iDTP].checkin.date]=hoursWorkedRounded;
            stats.daysCheckins[daysToProcess[iDTP].checkin.date]=checkin;
            totalHours+=hoursWorkedRounded;
            totalTrackedDays++;
        } catch(e){
            console.log("data missing or error, moving on...");
        }
    }
    stats.history.html = "<ul>"+stats.history.html;
    stats.history.html += "</ul>";

    stats.totalTrackedDays = totalTrackedDays;
    stats.totalWorkedHours = worklog.transcribeTime(worklog.roundToTwoDecimals(totalHours));
    stats.missingHours = worklog.transcribeTime(worklog.roundToTwoDecimals(totalTrackedDays*worklog.targetHours-totalHours));
    stats.averageHours = worklog.transcribeTime(worklog.roundToTwoDecimals(totalHours/totalTrackedDays));
	var totalCheckinArr = Object.entries(stats.daysCheckins);
	var totalCheckinUnixVal = totalCheckinArr.reduce(
    	function(totalVal,arrVal){
    		return totalVal+=moment("01/01/1970 "+arrVal[1],"DD/MM/YYYY hh:mm").unix();
    	},0
    );
    stats.averageCheckinTime = moment.unix(totalCheckinUnixVal/totalCheckinArr.length).format("hh:mm");
	
    stats.text+="\n"+"total tracked days: "+totalTrackedDays;
    stats.text+="\n"+"total worked hours: "+worklog.transcribeTime(worklog.roundToTwoDecimals(totalHours));
    stats.text+="\n"+"missing hours: "+worklog.transcribeTime(worklog.roundToTwoDecimals(totalTrackedDays*worklog.targetHours-totalHours));
    stats.text+="\n"+"average hours: "+worklog.transcribeTime(worklog.roundToTwoDecimals(totalHours/totalTrackedDays));
    stats.text+="\n"+"average checkin time: "+stats.averageCheckinTime;

    stats.html+="<ul class='worklog-stats-list'>";
    stats.html+="<li>total tracked days: <strong>"+totalTrackedDays+"</strong></li>";
    stats.html+="<li>total worked hours: <strong>"+worklog.transcribeTime(worklog.roundToTwoDecimals(totalHours))+"</strong></li>";
    stats.html+="<li>missing hours: <strong>"+worklog.transcribeTime(worklog.roundToTwoDecimals(totalTrackedDays*worklog.targetHours-totalHours))+"</strong></li>";
    stats.html+="<li>average hours: <strong>"+worklog.transcribeTime(worklog.roundToTwoDecimals(totalHours/totalTrackedDays))+"</strong></li>";
    stats.html+="<li>average checkin time: <strong>"+stats.averageCheckinTime+"</strong></li>";
    stats.html+="</ul>";

    //console.log(stats.text);

    worklog.stats = stats;
}

worklog.createSlot = function(log,date){
    if(date){
    	var m = moment(date); //para log manual
    } else {
    	var m = moment();
    }
    

    if(!log["years"]){
        log.years = {};
    }

    if(!log.years["y"+m.format("YYYY")]){
        log.years["y"+m.format("YYYY")] = {};
    }

    if(!log.years["y"+m.format("YYYY")]["m"+m.format("MM")]){
        log.years["y"+m.format("YYYY")]["m"+m.format("MM")] = {};
    }

    if(!log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")]){
        log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")] = {};
    }
}

worklog.getDayLogs = function(log){
    var m = moment();
    return log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")];
}

worklog.roundToTwoDecimals = function(floatVal){
    var parsedFloat = floatVal.toString().split(".");
    if(parsedFloat[1]){
        return parseFloat(parsedFloat[0]+"."+parsedFloat[1].substr(0,2));
    } else {
        return parseFloat(parsedFloat[0]);
    }
}


worklog.getLastNDays = function(totalDaysToProcess){
	var log = worklog.log;
	var processDayArr = [];
	var processYear = null;
	for (var iY in log.years){
		processYear = log.years[iY];
		for(var iM in processYear){
			for(var iD in processYear[iM]){
				processDayArr.push(processYear[iM][iD]);
				/*if(processDayArr.length >= totalDaysToProcess){
					return processDayArr;
				}*/
			}
		}
	}
	var returnArr = [];
	for (var i=totalDaysToProcess;i>0;i--){
		returnArr.push(processDayArr[processDayArr.length-i]);
	}
	return returnArr;
}

worklog.TRANSCRIBE_TIME_FULL = "transcribe1";
worklog.TRANSCRIBE_TIME_SIMPLE = "transcribe0";
worklog.transcribeTime = function(floatVal,mode){
    if(!mode) mode = worklog.TRANSCRIBE_TIME_SIMPLE;
    var valArr = floatVal.toString().split(".");
    var hours = valArr[0];
    var minutes = 0;
    var minutePlural = "s";
    if(valArr[1]){
        minutes = Math.round(parseFloat("0."+valArr[1])*60);
        if(minutes!=1) minutePlural = "s";
        else minutePlural = "";
    }
    var hourPlural = "";
    if(hours!=1) hourPlural = "s";
    else hourPlural = "";
    if(mode == worklog.TRANSCRIBE_TIME_FULL){
        return floatVal+" hs ("+hours+" hour"+hourPlural+" and "+minutes+" minute"+minutePlural+")";
    } else {
        return hours+"h "+minutes+"'";
    }
}


/**
 * GRAPHICS
 *
 * */
worklog.loadTemplate = function(targetContainerIdName,callback){
	$.get("worklog.html",function(response){
        $("#"+targetContainerIdName).append(response);
        callback();
    });
}
worklog.init = function(){

    worklog.view = {};
    worklog.model = {};

    worklog.view.stats = $("#worklog-stats");
    worklog.view.historyTotalTime = $("#worklog-history-totaltime");
    worklog.view.historyChecks = $("#worklog-history-checks");
    worklog.view.checkButton = $("#worklog-check");
    worklog.view.refreshButton = $("#worklog-refresh");
    worklog.view.checkAlert = $("#worklog-check-alert");
    worklog.view.manualCheckToggleButton = $("#worklog-manual-entry");
    worklog.view.manualCheckContainer = $("#worklog-manual-entry-container");
    worklog.view.historyShowTotaltimeButton = $("#worklog-history-totaltime-show");
    worklog.view.historyShowChecksButton = $("#worklog-history-checks-show");
    worklog.model.manualCheck = false;
    worklog.checkButtonHandler = function(){
    	if(worklog.model.manualCheck){
    		var timeStr = $("#worklog-manual-time").val();
    		var dateStr = $("#worklog-manual-date").val();
    		var type = $(".worklog-manual-type:checked").val();
    		if(timeStr && dateStr && type){
        		worklog.check(timeStr,dateStr,type);
    		} else {
    			alert("Hay campos incompletos");
    			return;
    		}
    	} else {
    		worklog.check();
    	}
        $(worklog.view.refreshButton).click();
        $(worklog.view.checkAlert).slideDown();
        $(worklog.view.checkAlert).delay(2000).slideUp();
    }
    

    $(worklog.view.checkAlert).hide();

    $(worklog.view.checkButton).unbind();
    $(worklog.view.checkButton).click(worklog.checkButtonHandler);
    
    $(worklog.view.historyShowTotaltimeButton).unbind();
    $(worklog.view.historyShowTotaltimeButton).click(function(){
    	$("#worklog-history-totaltime").show();
		$("#worklog-history-checks").hide();
		$(worklog.view.historyShowChecksButton).removeClass("active-tab");
		$(worklog.view.historyShowTotaltimeButton).removeClass("active-tab");
		$(worklog.view.historyShowTotaltimeButton).addClass("active-tab");
    });
    $(worklog.view.historyShowChecksButton).unbind();
    $(worklog.view.historyShowChecksButton).click(function(){
    	$("#worklog-history-checks").show();
		$("#worklog-history-totaltime").hide();
		$(worklog.view.historyShowTotaltimeButton).removeClass("active-tab");
		$(worklog.view.historyShowChecksButton).removeClass("active-tab");
		$(worklog.view.historyShowChecksButton).addClass("active-tab");
    });
    
    $(worklog.view.refreshButton).unbind();
    $(worklog.view.refreshButton).click(function(){
        worklog.makeStats();
        $(worklog.view.stats).html(worklog.stats.html);
        $(worklog.view.historyTotalTime).html(worklog.stats.history.html);
        var daysChecks = Object.entries(worklog.stats.daysCheckins);
        daysChecks.reverse();
        var daysChecksHtml = daysChecks.reduce(
        	function(totalVal,arrVal){
        		return totalVal+=arrVal.join(": ")+"<br>" 
        	},""
        );
        $(worklog.view.historyChecks).html(daysChecksHtml);
    });

    worklog.loadLog(function(){
        $(worklog.view.refreshButton).click();
    });
    
   	worklog.view.manualCheckToggleButton.unbind();
    worklog.view.manualCheckToggleButton.click(function(){
    	if(!worklog.model.manualCheck){
    		worklog.model.manualCheck = true;
    		$(worklog.view.manualCheckToggleButton).addClass("active-tab");
    		$(worklog.view.manualCheckContainer).slideDown();
    	} else {
    		worklog.model.manualCheck = false;
    		$(worklog.view.manualCheckToggleButton).removeClass("active-tab");
    		$(worklog.view.manualCheckContainer).slideUp();
    	}
    });
    
}