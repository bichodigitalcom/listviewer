/**
 * dailytasks version 2
 * Objective: To track and make sure you do your daily tasks as you did with an app before, but simpler. And to save the results.
  *
 * Dependencies:
 * -requires matlist.js and fileedit.js
 * -requires filemanager.php
 * -requires jQuery library
 * -saves results in a JSON
 *
 * */
 
 var dailytasks = {
 	line:-1,
 	defaultTasks:[
 			"-ejercicios respiratorios",
 			"-ducha",
 			"-locion cara 1/2",
 			"-elongar cuello 1/2",
 			"-elongar cuello 2/2",
 			"-ejercicios de brazos",
 			"-locion cara 2/2",
 			"-propafenona hci",
 			"-regar plantas",
 			"-caminar",
 			"-2en7 entrenamiento funcional"
 		]
 };
 
 
dailytasks.init = function(){
	dailytasks.setIndex();
	/*$("#dynamic-index").append("<button id='dt-execute' index='"+dailytasks.line+
	"' onclick='dailytasks.mainButtonClick()'>Daily Tasks</button>");
	$("#dt-execute").css("position", "absolute");
	$("#dt-execute").css("top", "50%");
	$("#dt-execute").css("right", "5%");*/
	$("#show-dailytasks").unbind();
	$("#show-dailytasks").click(function(){
		$("#popup1-close").click(); //first close the more-tools popup
		dailytasks.mainButtonClick()
	});
	if(dailytasks.line != -1){
		dailytasks.insertDailytasksButtons();
		dailytasks.loadPopupTemplate("dailytasks-container");
	}
}

dailytasks.mainButtonClick = function(){
	if(dailytasks.line == -1){
		dailytasks.promptCreate();
	} else {
    	$(".tarea[line="+dailytasks.line+"]")[0].scrollIntoView();
	}
}
 
dailytasks.insertDailytasksButtons = function(){
 	if(!$("#dt-reset-button-container").exists){
	 	$(dailytasks.titleElement).append("<div id='dt-reset-button-container'><button>Reset & save</button></div>");
	    $("#dt-reset-button-container button").unbind();
	    $("#dt-reset-button-container button").click(function(e){
	    	dailytasks.loadLogFirstThen(e,dailytasks.showResetDay);
	    });
	}
	if(!$("#dt-history-button-container").exists){
	 	$(dailytasks.titleElement).append("<div id='dt-history-button-container'><button>History</button></div>");
	    $("#dt-history-button-container button").unbind();
	    $("#dt-history-button-container button").click(function(e){
	    	dailytasks.loadLogFirstThen(e,dailytasks.showLog);
	    });
	}
}

 dailytasks.setIndex = function(){ //looks for dailytasks in the tasks list
 	$(".tarea").each(function (i, e) {
        if ($(e).find(".task-content").text() == "¶¶ DAILYTASKS") {
            dailytasks.line = $(e).attr("line");
            dailytasks.titleElement = $(e);
        }
    });
 }
 
 dailytasks.promptCreate = function(){
 	var createDailytasks = confirm("No se encuentra Dailytasks en esta lista, ¿Desea crearlo?");
 	if(createDailytasks){
 		if($("#lines-to-txt").hasClass("highlight-btn")){
 			alert("El documento tiene cambios sin guardar. Resuelva esto antes de crear Dailytasks y vuelta a intentarlo.");
 			return;
 		} else {
	 		//* add lines to raw file, save and reload
 			dailytasks.fileEditVal = $("#file-edit").val();
 			var manualIndexEnd = dailytasks.fileEditVal.search("¶¶¶¶¶¶¶¶¶¶¶¶");
 			if(manualIndexEnd != -1){
 				//adding entry to manual index if any
 				dailytasks.fileEditVal = dailytasks.fileEditVal.substring(0,manualIndexEnd)+"¶¶ DAILYTASKS\n"+dailytasks.fileEditVal.substring(manualIndexEnd);
 			}
 			$("#file-edit").val(dailytasks.fileEditVal+"\n¶¶ DAILYTASKS\n"+dailytasks.defaultTasks.join("\n")+"\n¶__\n\n");
 			delete dailytasks.fileEditVal;
 			
 			/*dailytasks.setIndex();
	 		$("#dt-execute").attr("index",dailytasks.line); //update the dailytasks line in the main button
	 		if(dailytasks.line == -1){
	 			alert("Hubo un error creando dailytasks en la lista de tareas... refresque sin guardar e intente nuevamente");
	 		} else {
	 			// matlist.js - mark document as changed
	 			$("#dummy-task").addClass("changed");
	        	$("#lines-to-txt").addClass("highlight-btn");
	 		}*/
 			
 			$(document).on(FILEEDIT_FILE_SAVED,function(){
 				location.reload();
 			});
 			savefile(); //fileedit.js 
 		}
 	}
 }
 

 dailytasks.saveAndResetTasks = function(targetDayString){ //TODO: change it to use dailytasks.getDailyTasks
 	//$(".tarea[line="+dailytasks.line+"]");
 	
 	//check if slot exists and it's not empty
 	let existed = dailytasks.checkSlot(dailytasks.log,targetDayString);
 	let doContinue = true;
   	if(existed){
   		doContinue = confirm("Ya existe una entrada para esa fecha ¿Desea reemplazarla?");
   	} else {
   		//check if date is earlier than latest date in log (do not allow that)
	 	if(	dailytasks.getLastNDays(1).length>0 && 
	 		dailytasks.getLastNDays(1)[0].date && 
	 		$("#dailytasks-reset-day-date").val()
	 	){
	 		let isNewDateOld = moment($("#dailytasks-reset-day-date").val()).isBefore(dailytasks.getLastNDays(1)[0].date);
	 		if(isNewDateOld){
	 			alert("Por el momento la fecha ingresada no puede ser anterior a la última fecha del historial (falta funcionalidad para reordenar una vez hecho esto). Si se puede reemplazar una fecha existente mal cargada.");
	 			return false;
	 		}
	 	}
   	}
   	if(!doContinue) return false;
 	//save the tasks
 	var dayDoneString = targetDayString.split("-").reverse().join("/");
 	var modified = false;
 	dailytasks.tasks = [];
 	dailytasks.currentTarea = dailytasks.titleElement.next();
 	for(var iCT = 300; iCT>0; iCT--){
 		if(dailytasks.currentTarea.find(".task-content").text()=="¶__"){
 			break;
 		} else {
 			let taskContent = dailytasks.currentTarea.find(".task-content").text();
 			//separo la tarea en marca y tarea en si
 			let taskTask = taskContent.substr(taskContent.search("-"));
			let taskMark = taskContent.substr(0,taskContent.search("-"));
			if(taskMark == "v" || taskMark == ""){
 				dailytasks.tasks.push(taskContent); //la guardo en el log como figura (cumplida o no cumplida)
 				if(taskMark == "v"){
 					dailytasks.currentTarea.find(".task-content").text(dayDoneString+taskTask); //si fue complida, le agrego la fecha en la que se hizo por última vez, sino la dejo intacta (como no cumplida)
 					modified = true;
 				}
			} else { //en este caso no se reconoce si fue cumplida o no, se toma como no cumplida y se deja intacta (con la fecha por ejemplo)
				dailytasks.tasks.push(taskTask); //la guardo en el log sin la marca (como no cumplida)
			}
			setStatusClass(dailytasks.currentTarea);
 			dailytasks.currentTarea = dailytasks.currentTarea.next();
 		}
 	}
 	dailytasks.logDailyTasks(dailytasks.tasks,targetDayString);
 	
 	/*
 	//reset the tasks
 	dailytasks.currentTarea = dailytasks.titleElement.next();
 	dailytasks.currentTareaContent = dailytasks.currentTarea.find(".task-content").text();
 	for(var iCT = 300; iCT>0; iCT--){
 		if(dailytasks.currentTareaContent=="¶__"){
 			break;
 		} else {
 			dailytasks.currentTarea.find(".task-content").text(
 				dailytasks.currentTareaContent.substring(dailytasks.currentTareaContent.search("-"))
 			);
 			setStatusClass(dailytasks.currentTarea);
 			dailytasks.currentTarea = dailytasks.currentTarea.next();
 			dailytasks.currentTareaContent = dailytasks.currentTarea.find(".task-content").text();
 		}
 	}
 	delete dailytasks.currentTareaContent;*/
 	delete dailytasks.currentTarea;
 	// matlist.js - mark document as changed
 	if(modified){
	 	$("#dummy-task").addClass("changed");
		$("#lines-to-txt").addClass("highlight-btn");
 	}
 	if(doContinue){ //fue ejecutado y no cancelado
 		$("#popup1-close").click(); //close the popup
 	}

 }
 
/**
 *  Get a list of the daily tasks on the listviewer (all tasks under this category)
 * */
 
 dailytasks.getDailyTasks = function(){
 	let arrDailyTasks = [];
 	dailytasks.currentTarea = dailytasks.titleElement.next();
 	for(var iCT = 300; iCT>0; iCT--){
 		if(dailytasks.currentTarea.find(".task-content").text()=="¶__"){
 			break;
 		} else {
			arrDailyTasks.push(dailytasks.currentTarea);
			setStatusClass(dailytasks.currentTarea);
 			dailytasks.currentTarea = dailytasks.currentTarea.next();
 		}
 	}
 	return arrDailyTasks;
 }
 
 /** 
  * LOG
  */
  
dailytasks.loadLogFirstThen = function(event,callback){ //preload log before calling functions that require the log to be present
	if(!dailytasks.log){
		dailytasks.loadLog(function(){
			callback(event);
		})
	} else {
		callback(event);
	}
}

dailytasks.loadLog = function(callback){
    $.post("data/dailytasks-log.json",null,function(response){
        dailytasks.log = response;
        topBarMessage("dailytasks log loaded");
        if(callback)callback()
    },'json').error(function(e){
    	if(e && e.status == 404){ //jamas va a pasar porque filemanager.php requiere que el archivo esté creado (no crea archivos, por cuestiones de seguridad... y vagancia jajaja)
    		topBarMessage("dailytasks log not found, creating a new one"); 
    		dailytasks.log = {};
    		if(callback)callback()
    	} else {
    		topBarMessage("error loading dailytasks log, status: "+e.status);
    	}
    });
}
 dailytasks.saveLog = function(log){ //sacado de worklog (listo)
    var postvars = {
        action: "save",
        body: JSON.stringify(log,null,"\t"),
        name: "dailytasks-log.json"
    }
    $.post("data/filemanager.php",postvars,function(response){
        return "ok";
    });
}

dailytasks.createSlot = function(log,date){ //sacado de worklog (listo)
    if(date){
    	var m = moment(date); //para log manual
    } else {
    	var m = moment();
    }
    

    if(!log["years"]){
        log.years = {};
    }

    if(!log.years["y"+m.format("YYYY")]){
        log.years["y"+m.format("YYYY")] = {};
    }

    if(!log.years["y"+m.format("YYYY")]["m"+m.format("MM")]){
        log.years["y"+m.format("YYYY")]["m"+m.format("MM")] = {};
    }

    if(!log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")]){
        log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")] = {};
    }
}

dailytasks.checkSlot = function(log,dateStr){
	var m = moment(dateStr);
	try{
		if(!log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")]){
			return false;
		} else {
			return true;
		}
	}catch(e){
		return false;
	}
}

dailytasks.logDailyTasks = function(tasks,dateStr){ //sacado de worklog pero editado bastante (listo)
    var log = dailytasks.log;
    dailytasks.createSlot(log,dateStr);
    var dayLog = {};
    var m = null
   	m = moment(dateStr); //TODO: make it parse the time and date string
    dayLog.date = m.format("YYYY-MM-DD");
    log.years["y"+m.format("YYYY")]["m"+m.format("MM")]["d"+m.format("DD")].tasks = tasks;
    dailytasks.saveLog(log);
}

dailytasks.getLastNDays = function(totalDaysToProcess){ //sacado de worklog, le tuve que agregar que incluyera la fecha del dia
	var log = dailytasks.log;
	var processDayArr = [];
	var processYear = null;
	for (var iY in log.years){
		processYear = log.years[iY];
		for(var iM in processYear){
			for(var iD in processYear[iM]){
				var newObj = {
					date:(iY+iM+iD).replace("y","").replace("m","-").replace("d","-"),
					tasks:processYear[iM][iD].tasks
				};
				processDayArr.push(newObj);
			}
		}
	}
	var returnArr = [];
	if(processDayArr.length<totalDaysToProcess)totalDaysToProcess=processDayArr.length;
	for (var i=totalDaysToProcess;i>0;i--){
		returnArr.unshift(processDayArr[processDayArr.length-i]);
	}
	return returnArr;
}


/**
 * GRAPHICS
 * 
 */

dailytasks.loadPopupTemplate = function(targetContainerIdName,callback){ //sacado de worklog pero muy editado (listo) --> por ahora no usado, fijate si no necesitás esta funcionalidad del callback, sacarla
	if(dailytasks.templateLoaded){
		if(callback)callback();
	} else {
		$.get("dailytasks.html",function(response){
	        $("#"+targetContainerIdName).append(response);
	        dailytasks.templateLoaded = true;
	        if(callback)callback();
	    });	
	} 
}
dailytasks.showLog = function(e){
	if(e) e.preventDefault();
	if(e) e.stopPropagation();
	
	$("#dailytasks-reset-day").hide();
	$("#dailytasks-history").show();
	
	/** la tabla se construye de arriba hacia abajo, recordar esto para programarla (fecha más reciente a la más antigua) */
	$("#dailytasks-popup #dailytasks-history-days").html("<div id='dt-stats-summary'></div><table></table><pre></pre>");
	$("#dailytasks-popup #dailytasks-history-days #dt-stats-summary").append(
		"te debo los stats... lo mas importante es que muestres el hueco más grande que tuviste"
	);
	let prevDay=null;
	dailytasks.getLastNDays(10).map((day,index)=>{
		if(day){
			if(prevDay && prevDay.date && (moment(prevDay.date).add(-1,"day").format("YYYY-MM-DD") != day.date) ){
				/** length+1 porque tasks no incluye la celda con la fecha */
				$("#dailytasks-popup #dailytasks-history-days table").append("<tr class='dailytasks-history-days-missing'><td colspan='"+(day.tasks.length+1)+"'>Período faltante</td></tr>");
			}
			if(prevDay && prevDay.tasks && prevDay.tasks.length > 0){
				for(var iPDT = 0; iPDT<prevDay.tasks.length; iPDT++){
					/** comparamos solo la tarea (sin la marca y sin updates sobre la misma, lo que está después de un símbolo ">"). Trim remueve whitespace para una mejor comparación */
					if(
						getTaskText(prevDay.tasks[iPDT],1) != getTaskText(day.tasks[iPDT],1)
					){
						/** length+1 porque tasks no incluye la celda con la fecha */
						$("#dailytasks-popup #dailytasks-history-days table").append("<tr><td></td><td class='dailytasks-history-days-changed-tasks' colspan='"+(day.tasks.length+1)+"'>Cambio en tareas</td></tr>");
						break;
					}
				}
			}
			prevDay = day;
			let arrDate = day.date.split("-");
			$("#dailytasks-popup #dailytasks-history-days pre").append(day.date+"<br>"+day.tasks.join("<br>")+"<br><br>");
			day.tasks = day.tasks.map(task=>task.substr(0,1)=="v"?task:" "+task);
			$("#dailytasks-popup #dailytasks-history-days table").append(
				"<tr><td>"+arrDate[2]+"/"+arrDate[1]+"</td>"+
				day.tasks.map((t)=>{
					let done = t.substr(0,t.search("-"))=="v"?"t-done":"t-notdone";
					let name = t.substr(t.search("-")+1);

					/** marca el primer renglon de la tabla si alguna tiene un dailytask activo y el mismo tiene la marca '!' */
					if(index == 0){
						let targetTaskText = name.split(">")[0];
						let foundMatchingDailyTask = dailytasks.findTask(targetTaskText, true)[0]; //solo me interesa el primer resultado
						if(foundMatchingDailyTask){
							foundMatchingDailyTask = $(foundMatchingDailyTask).find(".task-content").text(); //solo me interesa el texto, y findTask devuelve el objeto jquery del elemento HTML encontrado
							if(foundMatchingDailyTask.split("-")[0].indexOf("!")!=-1){
								done += " t-marked";
							}
						}
					}
					
					return "<td class=\""+done+"\">"+name+"</td>";
				})+"</tr>"
			);
		}
	});
	$("#dailytasks-history-days td").unbind();
	$("#dailytasks-history-days td").click(function(e){ //funcionalidad para editar la marca desde el log
		let targetTaskText = e.currentTarget.innerText.split(">")[0];
		console.log("targetTaskText: "+targetTaskText);
		let matchesFound = dailytasks.findTask(targetTaskText, true);
		if(matchesFound && matchesFound.length > 1){
			alert("Se encontró más de una dailytask que coincide con la seleccionada (debería haber una sola)");
		} else if (matchesFound && matchesFound.length == 1){
			let taskContent = matchesFound[0].find(".task-content").text(); //get the task content (full text)
			if(taskContent.split("-")[0].indexOf("!")==-1){ //chequea que no tenga la marca '!'
				editMark(matchesFound[0],"!");
				$(e.currentTarget).addClass("t-marked");
			}
		} else {
			alert("No se encontró una coincidencia en la lista de tareas actual");
		}
	});
	$("#dailytasks-history").show();
	$("#dailytasks-reset-day").hide();
	dailytasks.loadPopupTemplate("",function(){ //sacado de worklog (en matlist.js pero referido a esta funcionalidad)
	    $("#popup1-container").append($("#dailytasks-popup"));
        $("#popup1 h1").text("Dailytasks history (10 days)");
        $("#popup1").show();
        $("#popup-blocker").show();
	});
	
	
	
}

dailytasks.showResetDay = function(e){
	if(e) e.preventDefault();
	if(e) e.stopPropagation();
	
	$("#dailytasks-history").hide();
	$("#dailytasks-reset-day").show();

	$("#dailytasks-reset-day-date").val(moment().add("-1","day").format("YYYY-MM-DD"));
	
	
	$("#dailytasks-reset-day-submit").unbind();
	
	$("#dailytasks-reset-day-submit").click((e)=>{
		dailytasks.saveAndResetTasks($("#dailytasks-reset-day-date").val());
	});
	
	dailytasks.loadPopupTemplate("",function(){ //sacado de worklog (en matlist.js pero referido a esta funcionalidad)
	    $("#popup1-container").append($("#dailytasks-popup"));
        $("#popup1 h1").text("Dailytasks - Reset Day");
        $("#popup1").show();
        $("#popup-blocker").show();
	});
}

dailytasks.findTask = function(targetTaskText, exact){
	let foundMatches = [];
	if(!exact) return foundMatches; //this functionality isn't available yet
	//get list of tasks to compare texts
	let arrDailyTasks = dailytasks.getDailyTasks();
	let foundMatch = false;
	for (var iDT in arrDailyTasks){
		let taskContent = arrDailyTasks[iDT].find(".task-content").text(); //get the task content (full text)
		let taskTask = taskContent.substr(taskContent.search("-")+1); //get the task without the marks
		if(taskTask.split(">")[0] == targetTaskText){ //compare task without followup updates (everything before '>' char) and compare it to the log text for a match
			foundMatch = true;
			foundMatches.push(arrDailyTasks[iDT]);
		}
	}
	return foundMatches;
}

 