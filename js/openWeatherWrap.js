/**
 * OpenWeather Wrapper file
 * uses forecast5 endpoint
 * *requires moment.js
 */

var openWeatherWrap = {};
openWeatherWrap.weatherConstants = [
	{name:"Tornado", 		nameLang:"Tornado"	,icon:"wi-tornado"			,priority:true},
	{name:"Ash", 			nameLang:"Ceniza"		,icon:"wi-dust"				,priority:true},
	{name:"Snow", 			nameLang:"Nieve"		,icon:"wi-snowflake-cold"	,priority:true},
	{name:"Thunderstorm", 	nameLang:"Tormenta"	,icon:"wi-lightning"		,priority:true},
	{name:"Squall", 		nameLang:"Chubasco"	,icon:"wi-rain"				,priority:true},
	{name:"Rain", 			nameLang:"Lluvia"		,icon:"wi-rain-wind"		,priority:true},
	{name:"Drizzle", 		nameLang:"Llovizna"	,icon:"wi-snow-wind"		,priority:true},
	{name:"Mist", 			nameLang:"Bruma"		,icon:"wi-fog"							},
	{name:"Smoke", 			nameLang:"Humo"		,icon:"wi-smog"							},
	{name:"Haze", 			nameLang:"Neblina"	,icon:"wi-fog"							},
	{name:"Fog", 			nameLang:"Niebla"		,icon:"wi-fog"							},
	{name:"Sand", 			nameLang:"Arena"		,icon:"wi-dust"							},
	{name:"Dust", 			nameLang:"Polvo"		,icon:"wi-dust"							},
	{name:"Clear", 			nameLang:"Sol"		,icon:"wi-day-sunny"					},
	{name:"Clouds", 		nameLang:"Nubes"		,icon:"wi-cloud"						}
	];
openWeatherWrap.forecastLastLoaded = localStorage.getItem("openweather_wrap_last_updated");
openWeatherWrap.forecastLastLoadedAlt = localStorage.getItem("openweather_wrap_last_updated_alt");

openWeatherWrap.loadForecastOncePerDay = function(callback){
	var todayDate = moment().format("DD/MM/YYYY");
	if(openWeatherWrap.forecastLastLoaded != todayDate){
		openWeatherWrap.refreshWeatherData(callback);
	} else {
		callback();
	}
}
openWeatherWrap.refreshWeatherData = function(callback){
	$.get("https://api.openweathermap.org/data/2.5/forecast?"+
			"q=almagro,ar"+
			"&units=metric"+
			"&lang=es"+
			"&appid=e8705b5ecf5d7efe47c4b88c8300df11"
			,function(response){
		try{
			localStorage.setItem("openweather_wrap_weather_data",JSON.stringify(response));
			var todayDate = moment().format("DD/MM/YYYY");
			openWeatherWrap.forecastLastLoaded = todayDate;
			localStorage.setItem("openweather_wrap_last_updated",todayDate);
			openWeatherWrap.forecastLastLoadedAlt = moment().format("DD/MM HH:mm");
			localStorage.setItem("openweather_wrap_last_updated_alt",openWeatherWrap.forecastLastLoadedAlt);
			callback();
		} catch (jsonParseError){
			console.log("Error parsing weather JSON: "+response);
		}
	},"json");
}
openWeatherWrap.forecast = null;
openWeatherWrap.prepareWeatherData = function(){
	var forecast = openWeatherWrap.forecast;
	var weatherDataString = localStorage.getItem("openweather_wrap_weather_data");
	if(weatherDataString){
		weatherData = JSON.parse(weatherDataString);
		openWeatherWrap.rawData = weatherData;
		var weatherItem = null;
		var weatherItemDateStr = null;
		forecast = {};
		
		/**
		 * Iterate weather data array of 40 entries = 8 entries per day for 5 days every 3 hours
		 */
		for(var iWData in weatherData.list){
			weatherItem = weatherData.list[iWData];
			weatherItemDateStr = weatherItem.dt_txt.split(" ")[0];
			if(!forecast.hasOwnProperty(weatherItemDateStr)){
				forecast[weatherItemDateStr] = {max:[],min:[],description:[],wind:[],rain:[]};
			}
			forecast[weatherItemDateStr].max.push(weatherItem.main.temp_max);
			forecast[weatherItemDateStr].min.push(weatherItem.main.temp_min);
			forecast[weatherItemDateStr].description.push(weatherItem.weather[0].main);
			forecast[weatherItemDateStr].wind.push(weatherItem.wind.speed);
			if(weatherItem.hasOwnProperty("rain")){
				forecast[weatherItemDateStr].rain.push(weatherItem.rain["3h"]);
			} else {
				forecast[weatherItemDateStr].rain.push(0);
			}
		}
		/**
		 * Prepare daily summary
		 */
		for(var iForecast in forecast){
			forecast[iForecast].daily = {};
			forecast[iForecast].daily.max = Math.round(Math.max.apply(null,forecast[iForecast].max));
			forecast[iForecast].daily.min = Math.round(Math.min.apply(null,forecast[iForecast].min));
			forecast[iForecast].daily.description = (forecast[iForecast].description.filter(function(v,i){
				return forecast[iForecast].description.indexOf(v)===i;
			}).join(","));
			forecast[iForecast].daily.wind = Math.round(Math.max.apply(null,forecast[iForecast].wind))+"-"+Math.round(Math.min.apply(null,forecast[iForecast].wind));
			forecast[iForecast].daily.rain = forecast[iForecast].rain.reduce(function(prevVal,currentVal){return prevVal+currentVal},0);

			//generate translated description
			forecast[iForecast].daily.descriptionLang = [];
			var dailyDescriptionArr = forecast[iForecast].daily.description.split(",");
			var langItem = null;
			for(var iDArray in dailyDescriptionArr){
				langItem = openWeatherWrap.weatherConstants.find(function(elementLang){
					return elementLang.name == dailyDescriptionArr[iDArray]
				});
				forecast[iForecast].daily.descriptionLang.push(langItem.nameLang);
			}
			forecast[iForecast].daily.descriptionLang = forecast[iForecast].daily.descriptionLang.join(",");
		}
		/**
		 * Assign icon based on descriptions
		 */
		for(var iFore in forecast){
			weatherStats = openWeatherWrap.weatherConstants.slice();
	 		for(var iDescCompare in weatherStats){
	 			weatherStats[iDescCompare].count = 0;
				for(var iDesc in forecast[iFore].description){
		 			if(weatherStats[iDescCompare].name == forecast[iFore].description[iDesc]){
		 				weatherStats[iDescCompare].count++;
					}
				}
			}
			
			// first try finding any weather with priority (like storm or tornado) and use that
			var priorityFound = false;
			for(var iPriority in weatherStats){
				if(weatherStats[iPriority].priority && weatherStats[iPriority].count > 0){
					forecast[iFore].daily.icon = weatherStats[iPriority].icon;
					priorityFound = true;
				}
			}
			// if no priority weather found, use the weather with most occurrences
			if(!priorityFound){
				weatherStats.sort(function(a,b){
					if(a.count < b.count) return 1
					else if (a.count > b.count) return -1
					else return 0
				})
				forecast[iFore].daily.icon = weatherStats[0].icon;
			}
		}
		
	}
	openWeatherWrap.forecast = forecast;
}