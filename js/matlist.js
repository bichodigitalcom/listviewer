/**
 * Created by haswell on 22/01/17.
 */

var matlistData = {
    semanaIndexes:null
};
var dynamicIndex = null;
const MATLIST_ITEM_CHECKED = "MATLIST_ITEM_CHECKED";
const MATLIST_ITEM_UNCHECK_ALL = "MATLIST_ITEM_UNCHECK_ALL";
const MATLIST_ITEM_CLICKED = "MATLIST_ITEM_CLICKED";

function indexItemClicked(button) {
//    $(".tarea").get($(button).attr("index")).scrollIntoView();
    var indexNum = $(button).attr("index");
    $(".tarea[line="+indexNum+"]")[0].scrollIntoView();
    if($(".tarea[line="+indexNum+"]").hasClass("tarea-parent") && !$(".tarea[line="+indexNum+"]").hasClass("expanded")){
    	parentShowChildren(null,$(".tarea[line="+indexNum+"]")[0]);
    }
    $("#dynamic-index").hide();
    $("#popup-blocker").hide();
}

function moveItemsSelected(taskAbove,askAreChild,isChild) {
    var parentOriginalTab = null;
    var difference = 0;
    if($(".checked").length==0){
        alert("nothing is selected!");
    } else {
    	//gather selected tasks parents for processing afterwards
    	var selectedParents = [];
    	$(".checked").each(function(i,e){
    		var checkedTareaTab = $(e).attr("tab");
    		if(checkedTareaTab && parseInt(checkedTareaTab)>0){ // tarea is child
    			selectedParents.push(getParentOfTarea($(e)));
    		}
    	});
        var areChild = false;
        if(isChild) areChild = isChild;
        if(askAreChild)areChild = confirm("¿Insertar como hijo/s de esta tarea?",false);
        var insertAsFirstChildren = false;
    	if(getChildrenOfTarea(taskAbove).length>0 && areChild){
    		insertAsFirstChildren = confirm("** ¿Insertar como primer/os hijo/s?",false);
    	}
        if(getChildrenOfTarea(taskAbove).length>0 && !insertAsFirstChildren){
            $(".checked").insertAfter($(getLastChild(taskAbove)));
        } else {
            $(".checked").insertAfter($(taskAbove));
        }
        var targetTab = parseInt($(taskAbove).attr("tab"));
        if(areChild){
        	targetTab++;
        	if(!$(taskAbove).hasClass("tarea-parent")){ //mark new parent as parent if it's not already
        		$(taskAbove).addClass("tarea-parent");
        	}
        }
        $(".checked").each(function(i,e){
            if($(e).attr("selected-parent-original-tab")){
                parentOriginalTab = parseInt($(e).attr("selected-parent-original-tab"));
                difference = targetTab-parentOriginalTab;
                $(e).attr("tab",parseInt($(e).attr("tab"))+difference);
                //$(e).removeAttr("selected-parent-original-tab");
                $(e).attr("selected-parent-original-tab",targetTab);
            } else {
                $(e).attr("tab",targetTab);
            }
        });
        /** check if original parents are no longer parents and mark them */
        for(var iSP in selectedParents){
        	if(getChildrenOfTarea(selectedParents[iSP]).length == 0){
        		$(selectedParents[iSP]).removeClass("tarea-parent");
        	}
        }
        $("#dummy-task").addClass("changed");
        $("#lines-to-txt").addClass("highlight-btn");
        
    }
}
/** insertBefore must be true if you want the task before, if not choose false for after */
function moveTask(task,surroundingTask,insertBefore,askAreChild,isChild) {
    var parentOriginalTab = null;
    var difference = 0;
    $(task).show();
	
	//TODO: MOVER HIJOS TAMBIEN
	thisTaskChildren = getChildrenOfTarea(task, true);
	for(var iTTC in thisTaskChildren){
		$(thisTaskChildren[iTTC]).addClass("function-move-task-children");
	}
	parentOriginalTab = parseInt($(task).attr("tab"));
	
    if(insertBefore){
        $(task).insertBefore($(surroundingTask));
    } else {
        $(task).insertAfter($(surroundingTask));
    }

    var targetTab = parseInt($(surroundingTask).attr("tab"));
    if(!insertBefore){
        var areChild = isChild;
        if(askAreChild)areChild = confirm("¿Insertar como hijos de esta tarea?",false);
        if(areChild)targetTab++;
		$(task).attr("tab",targetTab);
    } else {
        $(task).attr("tab",targetTab);
    }
	
	difference = targetTab-parentOriginalTab; //check if this calc is ok
	var newChildTab = 0;
	var insertAfterTask = $(task);
	$(".function-move-task-children").each(function(i,e){
		$(e).insertAfter(insertAfterTask);
		$(e).removeClass("function-move-task-children");
		newChildTab = parseInt($(e).attr("tab"));
		newChildTab = newChildTab + difference;
		$(e).attr("tab", newChildTab);
		insertAfterTask = $(e);
	});
	
    $("#dummy-task").addClass("changed");
    $("#lines-to-txt").addClass("highlight-btn");
}

function hideBlankTasks(){
    $(".tarea").each(function(i,e){ //hide blank tareas
        if($(e).find(".task-content").text().length==0){
            $(e).hide();
        }
    });
}

function editMark(targetTask,mark){
    var taskText = $(targetTask).find(".task-content").text();
    if(taskText.search("-")<0){
        taskText = "-"+taskText;
        //alert("La línea seleccionada no tiene marca de tarea (\"-\", guión medio)");
    }
    var textArr = taskText.split("-");
    var marksArea = textArr[0];

    if(mark=="insert date"){
        var dateStr = $("#edit-mark input[type=date]").val();
        dateArr = dateStr.split("-");
        dateMark = dateArr[2]+"/"+dateArr[1]+"/"+dateArr[0];
        if(marksArea.search(/\d\d:\d\d/g)>=0){ //if it already has a time in it, insert it before the time
            var foundTimeMark = marksArea.match(/\d\d:\d\d/g);
            /** we will only use the first match, it shouldn't have more than one anyway */
            var splittedMarksArea = marksArea.split(/\d\d:\d\d/g);
            marksArea = splittedMarksArea[0]+dateMark+"_"+foundTimeMark[0]+splittedMarksArea[1];
            mark = ""; //we already inserted the text in marksArea in this case, no need to add anything else through mark var
        } else {
            mark = dateMark;
        }
    } else if (mark=="insert time"){
        var matches = marksArea.match(/\d\d\/\d\d\/\d\d\d\d/g);
        if(matches && matches.length>0){ //if a date is already in the mark
            mark = "_"+$("#edit-mark input[type=time]").val()
        } else {
            mark = $("#edit-mark input[type=time]").val()
        }
    }

    //TODO: (cancelled) make a complex system to identify existing marks to toggle between them, for now we just add them > EDIT: I just make a preview so you can see what you're doing
    if(mark == "clear"){
        marksArea = "";
    } else {
        marksArea = marksArea+mark;
    }
    textArr[0] = marksArea;
    $("#edit-mark-preview").text(textArr[0]);

    /** apply changes */
    $(targetTask).find(".task-content").text(textArr.join("-"));
    setStatusClass(targetTask);
    $("#dummy-task").addClass("changed");
    $("#lines-to-txt").addClass("highlight-btn");
}


/**
 * REGEX
 */

/* commented email regex was matching characters /&=? which was breaking for some urls includying @, and it entered a loop of conversion from hell  */
//var emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/g;
var emailRegex = /(?:[a-z0-9!#$%'*+_`{|}~-]+(?:\.[a-z0-9!#$%'*+_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/g;
var linkRegex = /((https?|ftp):\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/g;
//var telephoneRegex = /(?<![0-9])0?(\(?([0-9]{2})\)?-)?([0-9]{4})-([0-9]{4})(?![0-9])/g; //negative lookbehind not supported in mobile browsers...
//for negative lookbehind use js library: http://xregexp.com/
var telephoneRegex = /0?(\(?([0-9]{2})\)?-)?([0-9]{4})-([0-9]{4})(?![0-9\.a-zA-Z])/g;
var listFileRegex = /:::.*\.txt:::/g;

function convertTextToClickable(originalText){
	if(originalText.indexOf("@")>-1){
		originalText = processEmailRegex(originalText);
	}
	if(originalText.indexOf("://")>-1){
		originalText = processLinkRegex(originalText);
	}
	telephoneRegex.lastIndex = 0;
	if(telephoneRegex.test(originalText)){
		originalText = processTelRegex(originalText);
	}
	listFileRegex.lastIndex = 0;
	if(originalText.indexOf(".txt:::")){
		originalText = processListFileRegex(originalText);
	}
	
	
	return originalText;
	
}

function processEmailRegex(text){
	emailRegex.lastIndex = 0;
	var regexResult = emailRegex.exec(text);
	if(regexResult){
		var otArr = text.split(regexResult[0]);
		if(otArr[1].indexOf("@")>-1) otArr[1] = processEmailRegex(otArr[1]);
		return otArr.join("<a href='mailto:"+regexResult[0]+"'>"+regexResult[0]+"</a>");
	} else {
		return text;
	}
}
function processLinkRegex(text){
	linkRegex.lastIndex = 0;
	var regexResult = linkRegex.exec(text);
	if(regexResult){
		var otArr = text.split(regexResult[0]);
		if(otArr[1].indexOf("://")>-1) otArr[1] = processLinkRegex(otArr[1]);
		return otArr.join("<a href='"+regexResult[0]+"' target='_blank'>"+regexResult[0]+"</a>");
	} else {
		return text;
	}
}
function processTelRegex(text){
	telephoneRegex.lastIndex = 0;
	var regexResult = telephoneRegex.exec(text);
	if(regexResult){
		var otArr = text.split(regexResult[0]);
		telephoneRegex.lastIndex = 0;
		if(telephoneRegex.test(otArr[1])) otArr[1] = processTelRegex(otArr[1]);
		return otArr.join("<a href='tel:"+regexResult[0]+"'>"+regexResult[0]+"</a>");
	} else {
		return text;
	}
}
function processListFileRegex(text){
	listFileRegex.lastIndex = 0;
	var regexResult = listFileRegex.exec(text);
	if(regexResult){
		var otArr = text.split(regexResult[0]);
		if(otArr[1].indexOf(".txt:::")>-1) otArr[1] = processListFileRegex(otArr[1]);
		return otArr.join("<a href='?filename="+regexResult[0].slice(3,-3)+"' target='_blank'>"+regexResult[0]+"</a>");
	} else {
		return text;
	}
}





function setStatusClass(targetTask){
    var taskText = $(targetTask).find(".task-content").text();
    if(taskText.search("-")<0){
        taskText = "-"+taskText;
        //alert("La línea seleccionada no tiene marca de tarea (\"-\", guión medio)");
    }
    var textArr = taskText.split("-");
    var marksArea = textArr[0];
    if(marksArea.search("i")>=0){
        $(targetTask).addClass("status-info");
    } else {
        $(targetTask).removeClass("status-info");
    }
    if(marksArea.toLowerCase().search("v")>=0 || marksArea.toLowerCase().search("x")>=0){
        $(targetTask).addClass("status-done");
    } else {
        $(targetTask).removeClass("status-done");
    }
    if(marksArea.toLowerCase().search("!")>=0){
        $(targetTask).addClass("status-important");
    } else {
        $(targetTask).removeClass("status-important");
    }
}

function getChildrenOfTarea(tarea, includeSubChildren) {
    var returnArr = [];
    if(!tarea) return returnArr;
    var targetTarea = tarea
    var targetTareaTab = parseInt($(targetTarea).attr("tab"));
    var checkNext = true;
    var currentTarea = $(targetTarea);
    for  (var i=999; i>0; i--) {
        if (includeSubChildren && parseInt($(currentTarea).next().attr("tab")) > targetTareaTab) {
            returnArr.push($(currentTarea).next())
            currentTarea = $(currentTarea).next();
        } else if (!includeSubChildren && parseInt($(currentTarea).next().attr("tab")) == targetTareaTab + 1) {
            returnArr.push($(currentTarea).next())
            currentTarea = $(currentTarea).next();
        } else if (parseInt($(currentTarea).next().attr("tab")) <= targetTareaTab) {
            checkNext = false;
			break;
        } else {
			currentTarea = $(currentTarea).next();
		}
    }
	if(checkNext){
			alert("Revise la tarea \""+$(tarea).find(".task-content").text()+"\" porque hubo un problema obteniendo sus hijos");
			return [];
		}
    return returnArr;
}
function getParentOfTarea(tarea){
	previousTarea = $(tarea).prev();
	tareaTab = parseInt($(tarea).attr("tab"));
	if(previousTarea && tareaTab!=0){
		previousTareaTab = parseInt(previousTarea.attr("tab"));
		while(previousTarea[0] && tareaTab!=NaN && previousTareaTab!=NaN){
			if(previousTareaTab==tareaTab-1){
				return previousTarea;
			} else if (previousTareaTab<tareaTab-1){
				return false; //tabs are broken
			} else {
				previousTarea = $(previousTarea).prev();
				previousTareaTab = parseInt(previousTarea.attr("tab"));
			}
		}
	}
	return false;
}
function getTabZeroTareaOfTarea(tarea){ //returns the original holder of the whole tarea tree, which is tab zero
	if(parseInt($(tarea).attr("tab"))==0) return $(tarea);
	if(parseInt($(tarea).attr("tab"))>0){
		let parentOfTarea = tarea;
		for(var i=0;i<999;i++){
			parentOfTarea = getParentOfTarea(parentOfTarea);
			if(parseInt($(parentOfTarea).attr("tab"))==0){
				return parentOfTarea;
				break;
			} 
		}	
	} else {
		return null;
	}
}

function tareaClick(e) {
    $("#task-tools").find(".tool-show-children").hide();
    $("#task-tools").show();
    $(e.currentTarget).find(".tool-container").html($("#task-tools"));
    $(".tarea").removeClass("highlighted");
    $(e.currentTarget).addClass("highlighted");


    $(".tool-check").unbind();
    $(".tool-check").click(function (e) {
        e.stopPropagation();
        if ($(this).closest(".tarea").hasClass("checked")) {
            $(this).closest(".tarea").removeClass("checked");
            var childrenToCheck = getChildrenOfTarea($(this).closest(".tarea"), true);
            //var hasChildren = false;
            for (var iSubTarea in childrenToCheck) {
                //hasChildren = true;
                $(childrenToCheck[iSubTarea]).removeClass("checked");
                $(childrenToCheck[iSubTarea]).removeAttr("selected-parent-original-tab");
            }
            /*if(hasChildren){
                $(this).closest(".tarea").removeAttr("selected-parent");
                $(this).closest(".tarea").removeAttr("original-line");
            }*/
        } else {
            $(this).closest(".tarea").addClass("checked");
            var childrenToCheck = getChildrenOfTarea($(this).closest(".tarea"), true);
            //var hasChildren = false;
            for (var iSubTarea in childrenToCheck) {
                //hasChildren = true;
                $(childrenToCheck[iSubTarea]).addClass("checked");
                $(childrenToCheck[iSubTarea]).attr("selected-parent-original-tab",$(this).closest(".tarea").attr("tab"));
            }
            /*if(hasChildren){
                $(this).closest(".tarea").attr("selected-parent","true");
                $(this).closest(".tarea").attr("original-tab",$(this).closest(".tarea").attr("line"));
            }*/
        }
        $(document).trigger(MATLIST_ITEM_CHECKED,
	        {	
	        	task:$(this).closest(".tarea"),
	        	checked:$(this).closest(".tarea").hasClass("checked"),
	        	children:childrenToCheck
	        }
        );
        if($(".checked").length==0){
            //hide buttons for moving since there is none checked
            $("#bt-move").hide();
            $("#bt-unmark").hide();
            $("#task-tools").find(".tool-move").hide();
        } else {
            //show buttons for moving since there is some checked
            if($("#dynamic-index-show").is(":visible")) $("#bt-move").show(); //show only if there is an index
            $("#bt-unmark").show();
            $("#task-tools").find(".tool-move").show();
        }
    });

    $(".tool-edit").unbind();
    $(".tool-edit").click(function () {
        e.stopPropagation();
        $("#task-edit").show();
        var task = $(this).closest(".tarea");
        var tareaContent = $(task).find(".task-content").text();
        $(task).prepend($("#task-edit")[0]);
        $("#task-edit").find("textarea").val(tareaContent);
        $("#task-edit").find("#te-save").unbind();
        $("#task-edit").find("#te-save").click(function (e) {
            if ($(task).find(".task-content").text() != $("#task-edit").find("textarea").val()) {
                $(task).find(".task-content").html(convertTextToClickable($("#task-edit").find("textarea").val()));
                $(task).addClass("changed");
                $("#lines-to-txt").addClass("highlight-btn");
                setStatusClass($(task));
            }
            $("#task-edit").hide();
        });
        $("#task-edit").find("#te-revert").unbind();
        $("#task-edit").find("#te-revert").click(function (e) {
            $("#task-edit").find("textarea").val(tareaContent);
        });
        $("#task-edit").find("#te-cancel").unbind();
        $("#task-edit").find("#te-cancel").click(function (e) {
            $("#task-edit").hide();
        });
        $("#task-edit").find("#te-tab").unbind();
        $("#task-edit").find("#te-tab").click(function (e) {
            insertAtCaret("task-edit-textarea",tabChar);
        });
        $("#task-edit").find("#te-date").unbind();
        $("#task-edit").find("#te-date").click(function (e) {
            insertAtCaret("task-edit-textarea", moment().format("DD/MM/YYYY"));
        });
        //focus edit tarea
        $("#task-edit-textarea")[0].focus();
        $(".tarea.highlighted")[0].scrollIntoView();
    });

    $(".tool-show-children").unbind();
    $(".tool-show-children").click(parentShowChildren);

    $(".tool-move").unbind();
    $(".tool-move").click(function (e) {
        e.stopPropagation();
        moveItemsSelected($(this).closest(".tarea"),true);
    })

    $(".tool-add-task-below").unbind();
    $(".tool-add-task-below").click(addTaskBelow);

    $(".tool-set-mark").unbind();
    $(".tool-set-mark").click(function (e) {
        e.stopPropagation();
        var targetTarea = $(this).closest(".tarea");
        setMarkHandler(targetTarea);
    });

    $(".tool-delete").unbind();
    $(".tool-delete").click(function (e) {
        e.stopPropagation();
        var confirmed = confirm("¿Esta seguro que desea eliminar la tarea?"/*,"Eliminar", "Cancelar"*/);
        if(confirmed){
            var targetTaskToDelete = $(this).closest(".tarea");
            if($(targetTaskToDelete).hasClass("tarea-parent")){
                var taskChildren = getChildrenOfTarea(targetTaskToDelete,true);
                var confirmedChildren = confirm("¿Desea eliminar las subtareas también?"/*,"Eliminar", "Conservar"*/);
                if(confirmedChildren){
                    for(var iChildDel in taskChildren){
                        $(taskChildren[iChildDel]).remove();
                    }
                } else {
                    var newSubtaskTabValue = 0;
                    for(var iChildDel in taskChildren){
                        newSubtaskTabValue = parseInt($(taskChildren[iChildDel]).attr("tab"))-1;
                        $(taskChildren[iChildDel]).attr("tab",newSubtaskTabValue);
                    }
                }
            }
            
            //remove parent mark to task parent if it has no children left after this deletion
            var targetTaskToDeleteParent = getParentOfTarea(targetTaskToDelete);
            if(getChildrenOfTarea(targetTaskToDeleteParent).length == 1){
        		$(targetTaskToDeleteParent).removeClass("tarea-parent");
        	}
            
            //move task tooks to the next one to avoid deleting it
            $("#hidden-div").append($("#task-edit")[0]); //if the user edited the task, you need to move task-edit too
            $("#hidden-div").append($("#task-tools")[0]);

            targetTaskToDelete.remove();
            $("#dummy-task").addClass("changed");
            $("#lines-to-txt").addClass("highlight-btn");
        }
    });

    if ($(e.currentTarget).hasClass("tarea-parent")) { //SHOW TOOL SHOW CHILDREN IF PARENT
        $("#task-tools").find(".tool-show-children").show();
        if ($(e.currentTarget).hasClass("expanded")) {
            $("#task-tools").find(".tool-show-children").text("Collapse");
        } else {
            $("#task-tools").find(".tool-show-children").text("Expand");
        }
    }
    $("#task-tools").find(".tool-check").show();
    if ($(e.currentTarget).attr("selected-parent-original-tab")) { //HIDE BUTTON CHECK/UNCHECK IF ITS BEING PART OF A PARENT SELECTION (CREATES BUG IF UNSELECT)
    	$("#task-tools").find(".tool-check").hide();
    }
    $(".tool-launch-calendar").unbind();
    $(".tool-launch-calendar").click(function(e){
        e.stopPropagation();
        var tareaToCalendar = $(this).closest(".tarea");
        var errorAlertText = "La tarea seleccionada para ingresar en el calendario necesita como mínimo una fecha.";
        /** el codigo siguiente fue extraido de la funcion editMark el 17/03/2018 (fragmentos) para parsear el contenido de la tarea. Si hay una mejor manera de parsearlo actualizala por favor. Elegí no separar en funciones para reusar porque editMark es bastante claro como está, y no lo necesito en ningún otro lugar. */
        var taskText = $(tareaToCalendar).find(".task-content").text();
        if(taskText.search("-")<0){ //revisar si tarea tiene marca
            alert(errorAlertText);
            return;
        }
        var textArr = taskText.split("-");
        var marksArea = textArr[0];
        var foundDateMark = "";
        var foundTimeMark = "";
        if(marksArea.search(/\d\d\/\d\d\/\d\d\d\d/g)>=0){//revisar si tarea tiene fecha
            foundDateMark = marksArea.match(/\d\d\/\d\d\/\d\d\d\d/g)[0]; //solo pido la primera coincidencia (no debería haber más de todas formas)
            if(marksArea.search(/\d\d:\d\d/g)>=0){
                foundTimeMark = marksArea.match(/\d\d:\d\d/g)[0]; //idem date, solo pido la primera coincidencia
            }
        } else {
            alert(errorAlertText);
            return;
        }
        var foundDateMarkArr = foundDateMark.split("/").reverse();
        if(foundDateMarkArr[0].length==2){ //check if year has only two digits, and correct it
            foundDateMarkArr[0] = "20"+foundDateMarkArr[2]; //18 is 2018
        }
        var taskDate = foundDateMarkArr.join("");
        var taskTime = foundTimeMark.split(":").join("");
        var taskDescription = textArr[1].split(">")[0].split("<")[0];
//    	googleCalendarLaunch("20171004","1445","hacer tal cosa","","detalles de esa tal cosa");
    	googleCalendarLaunch(taskDate,taskTime,taskDescription,"","te debo los detalles...");

        /** apply c mark to task */
        $(tareaToCalendar).find(".task-content").text("c"+textArr.join("-"));
        setStatusClass(tareaToCalendar);
        $("#dummy-task").addClass("changed");
        $("#lines-to-txt").addClass("highlight-btn");
    });
    
    $(document).trigger(MATLIST_ITEM_CLICKED,{task:$(this).closest(".tarea")});

}

function setMarkHandler(targetTarea) {
    $("#hidden-div").append($("#popup1-container").children()); //clear the popup just in case;
    $("#popup1-container").append($("#edit-mark"));
    $("#edit-mark").find("button").unbind();
    $("#edit-mark").find("button").click(function(e){
        editMark(targetTarea,$(this).text());
    });
    $("#popup1 h1").text("Set Mark");
    $("#popup1").show();
    $("#popup-blocker").show();
    //extra: mark the "marks" already in the task
    var markPreviewArr = $(targetTarea).find(".task-content").text().split("-");
    var markPreview = "???";
    if(markPreviewArr.length>0){
        markPreview = markPreviewArr[0]+"-";
    }
    $("#edit-mark-preview").text(markPreview);
}


function parentShowChildren(e,tarea) {
	var targetTarea = null;
	if(e){
    	e.stopPropagation();
    	targetTarea = $(e.currentTarget).closest(".tarea");
	} else if (tarea) {
		targetTarea = tarea;
	}
    var collapsed = true;
    if ($(targetTarea).hasClass("expanded")) {
        collapsed = false;
        $(targetTarea).removeClass("expanded");
        $("#task-tools").find(".tool-show-children").text("Expand");
    } else {
        $(targetTarea).addClass("expanded");
        $("#task-tools").find(".tool-show-children").text("Collapse");
    }
    var targetTareaTab = parseInt($(targetTarea).attr("tab"));
    var checkNext = true;
    var currentTarea = $(targetTarea);
    while (checkNext) {
        if (parseInt($(currentTarea).next().attr("tab")) == targetTareaTab + 1) { //one tab higher we show it and move to the next
            if (collapsed) {
                $(currentTarea).next().show();
            } else {
                $(currentTarea).next().hide();
            }
            currentTarea = $(currentTarea).next();
        } else if (parseInt($(currentTarea).next().attr("tab")) > targetTareaTab + 1) { //more than one tab higher we ignore and move to the next (if
            if (!collapsed) {
                $(currentTarea).next().hide();
            }
            currentTarea = $(currentTarea).next();
        } else { //if the tab is lower or any other case, we end it
            checkNext = false;
        }
    }
}

function addTaskBelow(event,currentTarea,content) {
	if(!content) content = "";
    event.stopPropagation();
    if(event && !currentTarea){
        currentTarea = event.currentTarget.closest(".tarea");
    }
    //TODO: to find out the category of the task use .prev("index")
    var isChild = null;
    if($(currentTarea).hasClass("tarea-parent")){
        isChild = true; //if the tarea is parent already, force it to insert as a child (temporal fix so it doesn't break the parent status)
        //TODO: restore this confirm and if the tarea is parent and isChild is false, insert it after all its children but as a sibling
    } else {
        isChild = confirm("¿Insertar como subtarea de la actual?","subtarea","tarea");
    }
    var newTaskTabValue = parseInt($(currentTarea).attr("tab"));
    var newLineValue = "nl"+$(currentTarea).attr("line");
    if(isChild) newTaskTabValue++;
    var newTarea = $("<div class=\"tarea\" line=\"" + newLineValue + "\" tab=\"" + newTaskTabValue + "\"><span class='task-content'>-"+content+"</span><div class=\"tool-container\"></div></div>").insertAfter($(currentTarea));
    $(currentTarea).next().addClass("changed");
    $("#lines-to-txt").addClass("highlight-btn");
    if(isChild){
        if($(currentTarea).hasClass("tarea-parent")){
            if(!$(currentTarea).hasClass("expanded")){ //show children if we're adding a child (so we can see all of them, including the new one)
                $(currentTarea).find(".tool-container .tool-show-children").click();
            }
        } else {
            $(currentTarea).addClass("tarea-parent"); //if we added a new child, the parent tarea has to be a tarea-parent (if it wasn't already)
            $(currentTarea).addClass("expanded"); //in this instance the children are shown, and if the parent tarea wasn't a parent, we need to set it as expanded so the button expand/contract is right
        }
    }
    $(".tarea").unbind();
    $(".tarea").click(tareaClick);
    $(newTarea).click();
    /*setTimeout(function(newTarea){
        //$(newTarea).click();
        $(".tarea.highlighted").next().click();
    },500);*/
    //tareaClick({currentTarget:newTarea[0]});
//    $("#task-task-tools-first-row.tool-edit").click();
    //$(currentTarea).next(".tarea").click();
    $(newTarea).find(".tool-edit").click();
    $("#task-edit-textarea")[0].focus(); //.focus in pure javascript does focus. In jQuery is an event assignment.
    $(".tarea.highlighted")[0].scrollIntoView();


    /* lo de abajo no funca, necesito emplear otro método más directo para editar la tarea automaticamente
     $(currentTarea).next(".tarea").click();
     $(currentTarea).next(".tarea").find("#tool-edit").click();
     */
}
/** parses task content text and returns the task text without the mark, and optinally without everything behind the first ">" character (updates) */
function getTaskText(taskContentText, withoutUpdates){
	if(taskContentText){
		return taskContentText.substr(taskContentText.search("-")+1).split(">")[0].trim()
	} else return null;
}

var textArr;
var indexManualArr;
var tabChar = "	";
var openedTime = 0;

var bastardTasks = [];

$(document).ready(function(){
    $("#dynamic-index").hide();
    $("#popup-blocker").hide();
//    $("#general-tools").hide();
    $("#loading").stop().fadeOut(); //on android it just goes away and doesn't fade... maybe it's because it freezes because of the processing of the document
    moment.locale("es");
});

$(document).on(FILEEDIT_FILE_LOADED, function () {
	openedTime = moment().unix();
	document.addEventListener("visibilitychange", checkModify);


    /**
     * MAIN PARSER FUNCTION
     */

    /** FORMAT ELEMENTS START POSITION */
    $("#hidden-div").hide();
    $("#general-tools").show();
    $("#popup1").hide();
    $("#popup-blocker").hide();
    $("#popup1-close").unbind();
    $("#popup1-close").click(function(e){
        $("#popup1").hide();
        $("#popup-blocker").hide();
        $("#hidden-div").append($("#popup1-container").children());
    });
    $("#hidden-div").append($("#raw-edit"));

    /** MAIN PARSER */
    var text = $("textarea#file-edit").val();
    textArr = text.split("\n");
    var indexLastTab = 0;
    for (var iTab in textArr) { //ASSIGN TAB NUMBER
        indexLastTab = 0;
        while (textArr[iTab].substr(indexLastTab, 1) == tabChar) {
            indexLastTab++;
        }
        textArr[iTab] = [indexLastTab, textArr[iTab].substr(indexLastTab)];
    }
    $("#lines").html("")
    for (var i in textArr) {
        /*if(textArr[i].indexOf("debora")!=-1)*/
        $("#lines").append("<div class=\"tarea\" line=\"" + i + "\" tab=\"" + textArr[i][0] + "\"><span class='task-content'>" + convertTextToClickable(textArr[i][1]) + "</span><div class=\"tool-container\"></div></div>");
    }
    hideBlankTasks();

    /**
     * GENERAL BUTTONS
     */
    $("#lines-to-txt").click(function () {
        //TODO: disabled because it doesn't detect when you move or delete tasks
        /*if($(".changed").length==0){
            alert("No lines to save, nothing was changed or added.");
            return;
        }*/
        var newTXT = "";
        $(".tarea").each(function (i, e) {
            if($(e).attr("id")=="dummy-task") return true; //you have to do this to use a continue in an each loop
            //$("textarea#file-edit").append($(e).find(".task-content").html());
            var tabNum = parseInt($(e).attr("tab"));
            var tabs = "";
            while (tabNum > 0) {
                tabNum--;
                tabs += tabChar;
            }
            if(i!=0){
                newTXT += "\n"
            }
            newTXT += tabs + $(e).find(".task-content").text();
        });
        $("textarea#file-edit").val(newTXT);
        //SAVE THE ACTUAL FILE (using functions from fileedit.js) TODO: use a event to save it instead of injecting dependency to fileedit.js here
        savefile(); //fileedit.js
        $("#lines-to-txt").removeClass("highlight-btn");
    });
    $("#show-raw").unbind();
    $("#show-raw").click(function(e){
        $("#popup1-close").click(); //first close the more-tools popup
        $("#popup1-container").append($("#raw-edit"));
        $("#popup1 h1").text("Raw Edit");
        $("#popup1").show();
        $("#popup-blocker").show();
    });

    $("#general-tool-more").unbind();
    $("#general-tool-more").click(function(){
        $("#popup1-container").append($("#show-more-tools"));
        $("#popup1 h1").text("More Tools");
        $("#popup1").show();
        $("#popup-blocker").show();
    });
    $("#focus-highlighted").unbind();
    $("#focus-highlighted").click(function(){
        if($(".highlighted").length>0){
            $(".highlighted").get(0).scrollIntoView();
        }
    });

    $("#show-file-message").unbind();
    $("#show-file-message").click(function(e){
        $("#popup1-close").click(); //first close the more-tools popup
        afterMessage(); //from fileedit.js
    });

    $("#launch-online-backup-load").unbind();
    $("#launch-online-backup-load").click(function(){
        var filePath = Utils.window.getURLParameter("filename");
        window.open("fetch/?filepath="+filePath);
    });
    $("#launch-online-backup-save").unbind();
    $("#launch-online-backup-save").click(function(){
        var filePath = Utils.window.getURLParameter("filename");
        window.open("fetch/putclient.php?filepath="+filePath);
    });

    $("#gastos-shortcut").hide(); //hide this button until we found GASTOS section (if it is available)


    /**
     *  MARK PARENTS BASED ON TAB NUMBERS AND HIDE CHILDREN
     **/
    $(".tarea").each(function (i, e) {
        if ($(e).attr("tab") != 0) {
            $(e).hide();

            if ($(e).prev().attr("tab") < $(e).attr("tab")) { //mark parent tarea as such
                //$(e).prev().attr("parent","true");
                $(e).prev().addClass("tarea-parent");
            }
            /** check if it's a bastard task (by tab of children being greater by more than 1 compared to parent) */
            if ( parseInt($(e).attr("tab"))-parseInt($(e).prev().attr("tab"))>1 ) {
                bastardTasks.push($(e))
            }
        }
        /** check if it's a bastard task (by parent being blank) */
        if($(e).find(".task-content").text()==""){
        	if(parseInt($(e).next().attr("tab"))>0) bastardTasks.push($(e).next())
        }
        //$(e).css("margin-left", parseInt($(e).attr("tab")) * 20 + "px");
        setStatusClass($(e)); //CAN BE USED IN OTHER .tarea LOOPS, NOT RELATED TO MARKING PARENTS
    });
    if(bastardTasks.length > 0){
    	alert("Existen tareas huérfanas. Revise la consola para encontrarlas (serán impresas allí)");
    	console.log("==\n== TAREAS HUÉRFANAS\n==");
    	$(bastardTasks).each(function(i,e){
    		console.log("Linea "+(parseInt($(e).attr("line"))+1)+": "+$(e).find(".task-content").text());
    	})
    	console.log("==\n== FIN TAREAS HUÉRFANAS\n==");
    }

    /**
     *  BUILD DYNAMIC INDEX
     **/
//                var indexFound = false;
//                var previousLineWasIndex = false;
    var manualIndex = null;
    var currentManualIndexLine = null;
    /** first we capture the existing manual index if any */
    $(".tarea").each(function (i, e) {
        if ($(e).find(".task-content").text() == "¶¶ INDICE") {
            manualIndex = [$(e).attr("line")];
            currentManualIndexLine = $(e);
        }
    });
    if (manualIndex && manualIndex.length > 0) {
        while ($(currentManualIndexLine).next().find(".task-content").text().substr(0, 1) == "¶") {
            currentManualIndexLine = $(currentManualIndexLine).next();
//                        manualIndex.push(currentManualIndexLine);
            manualIndex.push(currentManualIndexLine.attr("line"));
        }
    }
    /*if($(".tarea .task-content:contains('¶¶ INDICE')").length>0){
     manualIndex = [$(".tarea .task-content:contains(¶¶ INDICE)")];
     currentManualIndexLine = manualIndex[0];
     while($(currentManualIndexLine).next().find(".task-content").html().substr(0,1)=="¶"){
     currentManualIndexLine = $(currentManualIndexLine).next();
     manualIndex.push(currentManualIndexLine);
     }
     }*/
    dynamicIndex = [];
    matlistData.semanaIndexes = { /** esto es para usar posteriormente en otra funcion (la que arma la semana, updateUpcomingWeek) */
        semanaCategoryTitle:null,
        lunes:null,
        martes:null,
        miercoles:null,
        jueves:null,
        viernes:null,
        sabado:null,
        domingo:null,
        nextweek:null
    };
    var insideSemana = false;
    var semanaMatchArr = ["-Lunes", "-Martes", "-Miércoles", "-Jueves", "-Viernes", "-Sábado", "-Domingo","-Next Week"];
    var indexItemSemana = null;
    var indexItem = null;
    var tareaString = "";
    $(".tarea").each(function (i, e) {
        //if($(e).find(".task-content").text().substr(0,1)=="¶" && manualIndex.indexOf($(e))==-1){
        if ($(e).find(".task-content").text().substr(0, 1) == "¶" && ((manualIndex && manualIndex.indexOf($(e).attr("line")) == -1)||!manualIndex) && $(e).find(".task-content").text() != "¶__") {
            dynamicIndex.push($(e));
            indexItem = "<br><button index=\"" + i + "\" onclick=\"indexItemClicked(this)\">" + $(e).find(".task-content").text() + "</button>";
            $("#dynamic-index").find("#dynamic-index-container").append(indexItem);
            if ($(e).text() == "¶¶ SEMANA") {
                insideSemana = true;
                matlistData.semanaIndexes.semanaCategoryTitle = $(e).attr("line");
            } else if ($(e).text() == "¶__") {
                insideSemana = false;
            }
        }
        if (insideSemana) {
            tareaString = $(e).find(".task-content").text().toString();
            for (var iSemana in semanaMatchArr) {
                if (tareaString.search(semanaMatchArr[iSemana]) >= 0) {
                    indexItemSemana = "<br><button index=\"" + i + "\" onclick=\"indexItemClicked(this)\">" + $(e).find(".task-content").text() + "</button>";
                    $(e).addClass("semana-dia");
                    $("#dynamic-index").find("#dynamic-index-container").append(indexItemSemana);
                    switch(iSemana){ /** semanaIndexes es para usar con updateUpcomingWeek, no hace nada en esta funcion */
                        case "0":
                            matlistData.semanaIndexes.lunes = $(e).attr("line");
                            break;
                        case "1":
                            matlistData.semanaIndexes.martes = $(e).attr("line");
                            break;
                        case "2":
                            matlistData.semanaIndexes.miercoles = $(e).attr("line");
                            break;
                        case "3":
                            matlistData.semanaIndexes.jueves = $(e).attr("line");
                            break;
                        case "4":
                            matlistData.semanaIndexes.viernes = $(e).attr("line");
                            break;
                        case "5":
                            matlistData.semanaIndexes.sabado = $(e).attr("line");
                            break;
                        case "6":
                            matlistData.semanaIndexes.domingo = $(e).attr("line");
                            break;
                        case "7":
                            matlistData.semanaIndexes.nextweek = $(e).attr("line");
                            break;
                    }

                }
            }
        }
    });
    $("#dynamic-index-show").click(function () {
        $("#dynamic-index").show();
        $("#popup-blocker").show();
    });
    $("#di-close").click(function () {
        $("#dynamic-index").hide();
        $("#popup-blocker").hide();
    });
    $("#dynamic-index").hide(); //also put outside "FILE_LOADED" in case file doesn't load
    $("#popup-blocker").hide();
    //-- finally after building the index, clone it to make the "move tasks selected to" menu
    var diDuplicate = $("#dynamic-index").clone();
    $(diDuplicate).attr("id","dynamic-index-moveto");
    $(diDuplicate).find("#dynamic-index-container").attr("id","dynamic-index-moveto-container");
    $(diDuplicate).find("#di-close").attr("id","di-moveto-close");
    $(diDuplicate).insertAfter($("#dynamic-index"));
    $(diDuplicate).find("button").unbind();
    $(diDuplicate).find("button").click(function(e){
        var indexNum = $(this).attr("index");
        var targetTareaToMoveTo = $(".tarea[line="+indexNum+"]");
        if($(targetTareaToMoveTo).hasClass("semana-dia")){
            moveItemsSelected(targetTareaToMoveTo,false,true);
        } else {
            moveItemsSelected(targetTareaToMoveTo,false,false);
        }
        $("#dynamic-index-moveto").hide();
    });
    $("#di-moveto-close").click(function () {
        $("#dynamic-index-moveto").hide();
        $("#popup-blocker").hide();
    });
    $("#dynamic-index-moveto").hide();
    $(dynamicIndex).each(function(i,e){
        $(e).addClass("category");
    });
    
    if(!checkListIntegrity(manualIndex,dynamicIndex)){
    	$("#integrity-warning").show();
    	alert("Falló el chequeo de integridad del archivo. Revise la lista porque es probable que esté corrupta. Puede recuperar los datos en la carpeta backup.")
    } else {
    	$("#integrity-warning").hide();
    };

    /**
     * ADD BUTTON FOR GASTOS
     */
    var catchNextCategoryEnd = false;
    $(".tarea").each(function (i, e) {
        if ($(e).find(".task-content").text().substr(0, 1) == "¶" && (manualIndex && manualIndex.indexOf($(e).attr("line")) == -1)) {
            if ($(e).text().search("¶¶ GASTOS")!=-1) {
                catchNextCategoryEnd = true;
            } else if ($(e).text() == "¶__" && catchNextCategoryEnd) {
                $("<div id='gastos-button-container'><button>NUEVO</button></div>").insertBefore($(e));
                $("#gastos-button-container button").unbind();
                $("#gastos-button-container button").click(openNewGasto);
                $("#gastos-shortcut").show();
                $("#gastos-shortcut").unbind();
                $("#gastos-shortcut").click(openNewGasto);
                catchNextCategoryEnd = false;
            }
        }
        if(	$(e).find(".task-content").text().indexOf("█")!=-1||
        	$(e).find(".task-content").text().indexOf(" ")!=-1
        	){
        		$(e).addClass("list-graphic");
        	}
    });
    
    /**
     * BUILD DOCUMENT CONFIG
     */
    
    var catchNextCategoryEnd = false;
    matlistData.config = {};
    $(".tarea").each(function (i, e) {
    	
        if (
        	$(e).find(".task-content").text().substr(0, 1) == "¶" &&
        	(!manualIndex || (manualIndex && manualIndex.indexOf($(e).attr("line")) == -1))
        ) {
            if ($(e).text().search("¶¶ CONFIG")!=-1) {
                catchNextCategoryEnd = true;
            } else if ($(e).text() == "¶__" && catchNextCategoryEnd) {
                catchNextCategoryEnd = false;
                return;
            }
        }
        if(catchNextCategoryEnd && $(e).text().indexOf("¶")==-1) {
        	//separo el texto de la tarea por el primer guion, hasta el guion es el valor de la variable de configuron, desde el guion es el nombre de la variable, asi se respeta estructura de una tarea simple
          	matlistData.config[$(e).text().substr($(e).text().search("-")+1)] = $(e).text().substr(0,$(e).text().search("-"));
        }
    });

    /** ADD BUTTON FOR FORMATTING THE WEEK */
    var nextWeekIndex = matlistData.semanaIndexes.nextweek;
    var currentTarea = $(".tarea[line="+nextWeekIndex+"]").next();
    for(var iTarea = 0; iTarea<100; iTarea++){
        if($(currentTarea).find(".task-content").text() == "¶__"){
            break;
        } else {
            currentTarea = $(currentTarea).next();
        }
    }
    $("<div id='scan-schedule-button-container' onclick='scanScheduledTasks()'><button>ESCANEAR AGENDA</button></div>").insertBefore($(currentTarea));
    $("<div id='newweek-button-container' onclick='updateUpcomingWeek()'><button>FORMATEAR SEMANA NUEVA</button></div>").insertBefore($(currentTarea));

    /** CONTINUE */

    $("#gastos-button-container button").unbind();
    $("#gastos-button-container button").click(openNewGasto);

    $("#gastos-shortcut").unbind();
    $("#gastos-shortcut").click(openNewGasto);

    $("#load-files").unbind();
    $("#load-files").click(function(){
        var postvars = {
            action: "filelist"
        }
        $.post("filemanager.php",postvars,function(response){
            if(response.hasOwnProperty("status") && response.status == "ok"){
                $("#files-container").html("");
                if(response.files.length > 0 ){
                    for(var i in response.files){
                        $("#files-container").append("<button class='button-open-file' file='"+response.files[i]+"'>"+response.files[i]+"</button><br>");
                    }
                    $(".button-open-file").unbind();
                    $(".button-open-file").click(function(e){
                        window.open("?filename="+$(e.currentTarget).attr("file"));
                    });
                    $("#popup1-close").click(); //first close the more-tools popup
                    $("#popup1-container").append($("#choose-file"));
                    $("#popup1 h1").text("Open TXT file");
                    $("#popup1").show();
                    $("#popup-blocker").show();
                } else {
                    alert("There are no text files to open in this directory");
                }
            }
        },"json");
    });
    
    $("#more-open-reload-cache").show();
	$("#more-open-reload-cache").unbind();
	$("#more-open-reload-cache").click(function(e){
		location.reload(true);
	});

    /**
     * BULK TOOLS
     */
    $("#bt-move").click(function () {
        if($(".checked").length==0){
            alert("nothing is selected!");
        } else {
            $("#dynamic-index-moveto").show();
            $("#popup-blocker").show();
        }
    });
    $("#bt-unmark").click(btUnmark);
    $("#bt-filter").click(function(){

        $("#hidden-div").append($("#popup1-container").children()); //clear the popup just in case;
        $("#popup1-container").append($("#filter-popup"));


        $("#fp-filter").unbind();
        $("#fp-filter").click(function(){
            $(".tarea").show(); //expand all
            $(".tarea").each(function(i,e){
                if(!$(this).find(".task-content").text().match(new RegExp($("#fp-input-text").val(),"ig")))
                    $(e).hide()
            });
            $("#bt-filter-cancel").show();
            $("#popup1").hide();
            $("#popup-blocker").hide();

        });
        $("#fp-mark-filter").unbind();
        $("#fp-mark-filter").click(function(){
            $(".tarea").show(); //expand all
            $(".tarea").each(function(i,e){
                var taskText = $(this).find(".task-content").text();
                if(taskText.search("-")<0){
                    $(e).hide();
                    return;
                }
                var textArr = taskText.split("-");
                var marksArea = textArr[0];
                if(marksArea.toLowerCase().search($("#fp-input-text").val().toLowerCase())<0)$(e).hide();
            });
            $("#bt-filter-cancel").show();
            $("#popup1").hide();
            $("#popup-blocker").hide();
        });

        $("#popup1 h1").text("Filter tasks");
        $("#popup1").show();
        $("#popup-blocker").show();

        /*var query = prompt("buscar fragmento:");
        $(".tarea").show(); //expand all
        $(".tarea").each(function(i,e){if($(this).text().search(query)<0)$(e).hide()});*/

    });
    $("#bt-filter-cancel").click(function(){
        $(".tarea[tab=0]").show();
        hideBlankTasks();
        $("#bt-filter-cancel").hide();
    });

    //hide these since when you start there is no task selected
    $("#bt-move").hide();
    $("#bt-unmark").hide();
    $("#task-tools").find(".tool-move").hide();
    //default hidden buttons
    $("#bt-filter-cancel").hide();

    /**
     *  TASK TOOLS
     */
    $("#task-tools").hide();
    $("#task-edit").hide();
    $(".tarea").click(tareaClick);
    
    
    /**
     *  CONFIG apply
     */
    //show worklog
    if(	matlistData.config.hasOwnProperty("worklog") &&
    	matlistData.config.worklog.indexOf("v")!=-1){
    	$("#show-worklog").show();
    	$("#show-worklog").unbind();
	    $("#show-worklog").click(function(e){
	    	worklog.loadTemplate("worklog-container",function(){
	    		$("#popup1-close").click(); //first close the more-tools popup
		        $("#popup1-container").append($("#worklog-popup"));
		        $("#popup1 h1").text("WorkLog");
		        $("#popup1").show();
		        $("#popup-blocker").show();
		        worklog.init();
	    	});
	    });
    }
    //apply colors
    if(	matlistData.config.hasOwnProperty("generalToolsColor") &&
    	matlistData.config.generalToolsColor.length>2){
        $("#general-tools").css("background-color",matlistData.config.generalToolsColor);
    }
    if(	matlistData.config.hasOwnProperty("bulkToolsColor") &&
    	matlistData.config.bulkToolsColor.length>2){
        $("#bulk-tools").css("background-color",matlistData.config.bulkToolsColor);
    }
    
    //apply weather forecast to week
    if(matlistData.semanaIndexes.semanaCategoryTitle){
	    openWeatherWrap.loadForecastOncePerDay(function(){
			displayWeatherData();
			$("#more-open-week-forecast").show();
			$("#more-open-week-forecast").unbind();
			$("#more-open-week-forecast").click(function(e){
				showFullWeekForecastPopup(e);
			});
		});
		
    } else {
    	$("#more-open-week-forecast").hide();
    }
    
    //show exercise
    if(	matlistData.config.hasOwnProperty("exercise") &&
    	matlistData.config.exercise.indexOf("v")!=-1){
    	exercise.init();
    }
    //show dailytasks
    if(	matlistData.config.hasOwnProperty("dailytasks") &&
    	matlistData.config.dailytasks.indexOf("v")!=-1){
    	$("#show-dailytasks").show();
    	dailytasks.init();
    }
});

$(document).on(FILEEDIT_FILE_SAVED,function(e,fileData){
    $(".changed").removeClass("changed");
    $("#lines-to-txt").removeClass("highlight-btn");
    openedTime = fileData.modified;
});

//source: http://stackoverflow.com/questions/1064089/inserting-a-text-where-cursor-is-using-javascript-jquery (this one works perfectly, the jquery one didn't)
function insertAtCaret(areaId, text) {
    var txtarea = document.getElementById(areaId);
    if (!txtarea) { return; }

    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
        "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
    } else if (br == "ff") {
        strPos = txtarea.selectionStart;
    }

    var front = (txtarea.value).substring(0, strPos);
    var back = (txtarea.value).substring(strPos, txtarea.value.length);
    txtarea.value = front + text + back;
    strPos = strPos + text.length;
    if (br == "ie") {
        txtarea.focus();
        var ieRange = document.selection.createRange();
        ieRange.moveStart ('character', -txtarea.value.length);
        ieRange.moveStart ('character', strPos);
        ieRange.moveEnd ('character', 0);
        ieRange.select();
    } else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }

    txtarea.scrollTop = scrollPos;
}

/** notify of changes not being saved (does not display the text but it is required for it to work correctly??) */
$(window).bind('beforeunload', function(){
    if($(".changed").length>0){
        return "There are unsaved changed, do you still want to leave?";
    }
});

function btUnmark() {
    $(".checked").removeAttr("selected-parent-original-tab"); //attr designed to move children
    $(".checked").removeClass("checked");
    //hide buttons for moving since there is none checked after button press
    $("#bt-move").hide();
    $("#bt-unmark").hide();
    $("#task-tools").find(".tool-move").hide();
    $(document).trigger(MATLIST_ITEM_UNCHECK_ALL);
}

function openNewGasto(){
    $("#hidden-div").append($("#popup1-container").children()); //clear the popup just in case;
    $("#popup1-container").append($("#new-gasto-popup"));
    $("#popup1 h1").text("Add GASTO");
    //$("#ngp-financiado-cuotas").spinner();

    $("#ngp-prefix-deb").unbind();
    $("#ngp-prefix-deb").click(function(){
        $("#ngp-prefix").val("(deb)");
    });
    $("#ngp-prefix-mat").unbind();
    $("#ngp-prefix-mat").click(function(){
        $("#ngp-prefix").val("(mat)");
    });

    $("#ngp-add").unbind();
    $("#ngp-add").click(function(){

        if($("#ngp-price").val()=="" || $("#ngp-article").val()==""){
            alert("Debe ingresar un precio y un artículo");
            return;
        }

        //get checkboxes
        var financiadoStr = "";
        if($("#ngp-financ").is(":checked")) financiadoStr = $("#ngp-financ").val()+" ";
        var pendienteStr = "";
        if($("#ngp-pend").is(":checked")) pendienteStr = $("#ngp-pend").val()+" ";
        //build task
        var prefix = "";
        if($("#ngp-prefix").val().length>0){
            prefix = $("#ngp-prefix").val()+" ";
        }
        var suffix = "";
        if($("#ngp-suffix").val().length>0){
            suffix = " "+$("#ngp-suffix").val();
        }
        var gastoStr = pendienteStr+financiadoStr+prefix+$("#ngp-price").val()+tabChar+$("#ngp-article").val()+suffix;
        if($("#ngp-add-date").is(":checked")) gastoStr += tabChar+moment().format("DD/MM");
        var newLineValue = "g"+(parseInt($("#gastos-button-container").prev(".tarea").attr("line"))+1);
        var newTaskTabValue = 0;
        $("<div class=\"tarea changed\" line=\"" + newLineValue + "\" tab=\"" + newTaskTabValue + "\"><span class='task-content'>"+gastoStr+"</span><div class=\"tool-container\"></div></div>").insertBefore($("#gastos-button-container"));
        $("#ngp-message").slideDown();
        $("#ngp-message").delay(2000).slideUp();
        //clear some values
        $("#ngp-price").val("");
        $("#ngp-article").val("");
        $("#lines-to-txt").addClass("highlight-btn");
        $(".tarea").unbind();
        $(".tarea").click(tareaClick);
    });
    $("#ngp-clear").unbind();
    $("#ngp-clear").click(function(){
        $("#ngp-price").val("");
        $("#ngp-article").val("");
        if($("#ngp-pend").is(":checked"))$("#ngp-pend").click();
        if($("#ngp-financ").is(":checked"))$("#ngp-financ").click();
        $("#ngp-prefix").val("");
        $("#ngp-suffix").val("");
    });


    $("#ngp-message").hide();
    $("#popup1").show();
    $("#popup-blocker").show();
}

function updateUpcomingWeek(){
    var areYouSure = confirm("¿Esta seguro que desea continuar con el formateo de la semana nueva?");
    if(!areYouSure){
        return;
    }
    /*var stringsToFind = [
            "-Domingo        ",
            "-Lunes        ",
            "-Martes        ",
            "-Miércoles        ",
            "-Jueves        ",
            "-Viernes        "
        ];*/

    var checkDayComplete = 0;
    var targetTarea = null;
    var taskText = null;
    var textArr = null;
    var marksArea = null;
    for(var iDay in matlistData.semanaIndexes){
        if(iDay == "nextweek" || iDay ==  "semanaCategoryTitle"){
            continue; //solo tomamos en cuenta lun-dom
        }
        targetTarea = $(".tarea[line="+matlistData.semanaIndexes[iDay]+"]");
        taskText = $(targetTarea).find(".task-content").text();
        textArr = taskText.split("-");
        marksArea = textArr[0];
        if(marksArea == "v"){
            checkDayComplete++;
        }
    }
    if(checkDayComplete<7){
        var userChoseToContinue = confirm("El formateador de semana nueva detectó que no están marcados como hechos todos los días ¿Desea continuar?");
        if(!userChoseToContinue) return;
    }

    var todayDate = new Date();
    var dayOfWeek = todayDate.getDay();

    var childrenOfTarea = null;
    var childToManage = null;
    var doneIndex = $("#dynamic-index-container").find("button:contains('¶¶ DONE / PASAR')").attr("index");
    for(var iDay in matlistData.semanaIndexes){
        if( iDay ==     "nextweek"                  ||
            iDay ==     "semanaCategoryTitle"       ||
            iDay ==     "sabado"    && dayOfWeek==6 ||                  //no formateamos sábado si HOY es sábado
            iDay ==     "domingo"   && (dayOfWeek==0 || dayOfWeek ==6)  //no formateamos tampoco domingo si HOY es sábado o domingo
            ){
            continue; //solo tomamos en cuenta lun-vie
        }
        targetTarea = $(".tarea[line="+matlistData.semanaIndexes[iDay]+"]");
        childrenOfTarea = getChildrenOfTarea(targetTarea,false);
//        btUnmark(); //usaremos el mecanismo de seleccion para mover las tareas
        for(var iChild in childrenOfTarea){
            childToManage = childrenOfTarea[iChild];
            taskText = $(childToManage).find(".task-content").text();
            textArr = taskText.split("-");
            marksArea = textArr[0];

            if(marksArea == "v" || marksArea == "x"){
                //marcado como hecho, moverlo a Done
                moveTask($(childToManage),$(".tarea[line="+doneIndex+"]"),false,false,false);
            } else if (marksArea.indexOf("i") != -1 ){
                //marcado como info, dejarlo
            } else {
                //cualquier otro caso (-,L,W,M,N,! etc) , moverlo arriba del lunes (semana general) para que lo re-ordene
                moveTask($(childToManage),$(".tarea[line="+matlistData.semanaIndexes.lunes+"]"),true,false,false);
            }

        }

    }

    var datesArr = [];
    var dateCalc = null;

    if(dayOfWeek==0 || dayOfWeek==6){ //si es sabado o domingo formateamos toda la semana siguiente
        if(dayOfWeek==0){
            dateCalc = new Date();
            dateCalc.setDate(dateCalc.getDate());
            for(var iDates = 0; iDates<6; iDates++){ //si es domingo formateame de lunes a sábado
                dateCalc.setDate(dateCalc.getDate()+1);
                datesArr.push(dateCalc.getDate()+"/"+(dateCalc.getMonth()+1));
            }
        } else if(dayOfWeek==6){
            dateCalc = new Date();
            dateCalc.setDate(dateCalc.getDate()+1);
            for(var iDates = 0; iDates<5; iDates++){ //si es sábado formateame de lunes a viernes
                dateCalc.setDate(dateCalc.getDate()+1);
                datesArr.push(dateCalc.getDate()+"/"+(dateCalc.getMonth()+1));
            }
        }
    } else { //si es dia de la semana formateamos la semana que corre
        dateCalc = new Date();
        dateCalc.setDate(dateCalc.getDate()-dayOfWeek);
        for(var iDates = 0; iDates<8; iDates++){ //en este caso quiero toda la semana formateada
            dateCalc.setDate(dateCalc.getDate()+1);
            datesArr.push(dateCalc.getDate()+"/"+(dateCalc.getMonth()+1));
        }
    }

    var iDatesArr = 0;
    for(var iDay in matlistData.semanaIndexes){
        if(iDay == "nextweek" || iDay ==  "semanaCategoryTitle"){
            continue; //solo tomamos en cuenta lun-dom
        }
        targetTarea = $(".tarea[line="+matlistData.semanaIndexes[iDay]+"]");
        taskText = $(targetTarea).find(".task-content").text();
        textArr = taskText.split("-");
        if(datesArr[iDatesArr].length == 1){
            datesArr[iDatesArr] = "0"+datesArr[iDatesArr];
        }
        $(targetTarea).find(".task-content").text("-"+(textArr[1].split("\t")[0]+"\t")+datesArr[iDatesArr]);
        if(getChildrenOfTarea(targetTarea).length<1){
            $(targetTarea).removeClass("tarea-parent");
        }
        $(targetTarea).removeClass("status-done");
        iDatesArr++;
    }

    //1) ir a linea u obtener linea (index) de o Semana o -Lunes mediante el index dinamico
    //2) encontrar el -Lunes y revisar linea por linea hacia abajo para capturar todas las lineas que coinciden con el dia (o sacarlas directamente del indice dinamico)
    //4) parsear todos los dias, poniendo los completados en DONE y los incompletos arriba del Lunes
    //5) resetear los dias completados
    //6) reemplazar el dia del mes en cada Lun, Mar, Mie, etc...

    //marcar que hubo cambio
    $("#dummy-task").addClass("changed");
    $("#lines-to-txt").addClass("highlight-btn");

}

function scanScheduledTasks(){

    //TODO: deberíá chequear que la semana actual está formateada antes de manipularla!

    var currentScheduleTask = $(".task-content:contains('¶¶ AGENDA')").closest(".tarea").last();
    //var scheduleTasksArr = [];
    var thisWeekTasks = [];
    var nextWeekTasks = [];
    var thisWeekCount = 0;
    var nextWeekCount = 0;

    var marksArea = null;
    var foundDateMark = null;
    var foundDate = null;

    var mondayTarea = $(".tarea.semana-dia").first();
    var nextWeekTarea = $(".tarea.semana-dia").last();

    while($(currentScheduleTask).next().find(".task-content").text().substr(0, 1) != "¶"){
        currentScheduleTask = $(currentScheduleTask).next();
        //scheduleTasksArr.push(currentScheduleTask);
        //TODO: deberia transformar lo que estoy usando abajo en una funcion ya que lo uso también en otras partes (la saqué del parseador de calendario y eso lo saqué de otra parte también)
        marksArea = $(currentScheduleTask).find(".task-content").text().split("-")[0];
        if(	marksArea.search(/\d\d\/\d\d\/\d\d\d\d/g)!=-1 &&
        	parseInt($(currentScheduleTask).attr("tab"))==0
        ){
            foundDateMark = marksArea.match(/\d\d\/\d\d\/\d\d\d\d/g)[0]; //solo pido la primera coincidencia (no debería haber más de todas formas)
            foundDate = moment(foundDateMark.split("/").reverse().join("-")); //moment requiere formato ISO-8601, año4digitos-mes-dia
            /** es dentro de esta semana (contando a partir de ahora) */
            if(foundDate.isBefore(moment().add(7-parseInt(moment().format("e")),"days"))){
                $(currentScheduleTask).find(".task-content").text("!"+$(currentScheduleTask).find(".task-content").text()) //la marco como importante para verla
                $(currentScheduleTask).addClass("status-important");
                thisWeekTasks.push(currentScheduleTask);
                thisWeekCount++;
            } else if (foundDate.isBefore(moment().add(14-parseInt(moment().format("e")),"days"))){
                nextWeekTasks.push(currentScheduleTask);
                nextWeekCount++;
            }
        }
    }

    for(var iWeekTask in thisWeekTasks){
        moveTask(thisWeekTasks[iWeekTask], mondayTarea, true, false, false);
    }
    for(var iNextTask in nextWeekTasks){
        moveTask(nextWeekTasks[iNextTask], nextWeekTarea, false, false, true);
    }

    alert("Scan Schedule complete\n------\nResults:\n"+
        "Tasks for current week:"+thisWeekCount+"\n (check those above \"-Lunes\")\n"+
        "Tasks for next week:"+nextWeekCount+"\n (added inside \"-Next Week\")");

}

function displayWeatherData(){
	openWeatherWrap.prepareWeatherData();
	var forecast = openWeatherWrap.forecast;
	var forecastItem = null;
	for(var iForecast in forecast){
		forecastItem = forecast[iForecast].daily;
		var foreDateArr = iForecast.split("-");
		foreDateArr.splice(0,1);
		if(foreDateArr[0].substr(0,1)==="0") foreDateArr[0] = foreDateArr[0].substr(1,99);
		if(foreDateArr[1].substr(0,1)==="0") foreDateArr[1] = foreDateArr[1].substr(1,99);
		foreDateArr.reverse();
		
		var semanaDayFind = null;
		var semanaDayFindDate = null;
		var semanaItem = null;
		for(var iSemanaIndice in matlistData.semanaIndexes){
			semanaItem = $(".tarea[line="+matlistData.semanaIndexes[iSemanaIndice]+"]").find(".task-content");
			semanaDayFind = semanaItem.text();
			semanaDayFindDate = semanaDayFind.indexOf("/");
			if(semanaDayFindDate!=-1){
				semanaDayFindDate = semanaDayFind.split("\t")[1];
			}
			if(foreDateArr.join("/")==semanaDayFindDate){ //day matches
				$(semanaItem).siblings().each(function(i,e){
					if(
						$(e).hasClass("wi-separation") ||
						$(e).hasClass("wi") ||
						$(e).hasClass("week-weather")
					) $(e).remove();
				});
				$(
					generateWeatherDayElements(forecastItem,true)
				).insertAfter(semanaItem);
			}
		}
	}
	$(".weather_button").click(onClickWeatherIcon);
	$(".week-weather").click(showFullWeekForecastPopup);
	buildFullWeekForecast();
}

function buildFullWeekForecast(){
	$("#full-weather-forecast").html("Ultima actualización: "+openWeatherWrap.forecastLastLoadedAlt);
	$("#full-weather-forecast").append("<button id=\"force-refresh-forecast\" onclick=\"forceRefreshForecast()\">Refrescar datos</button>");
	var weatherItem = null;
	var day = null;
	var time = null;
	var timeStr = null;
	var dayForecast = null;
	var dayForecastHtml = "";
	for(var iList=0 in openWeatherWrap.rawData.list){
		weatherItem = openWeatherWrap.rawData.list[iList];
		timeStr = weatherItem.dt_txt.split(" ")[1].substr(0,5);
		if(day != weatherItem.dt_txt.split(" ")[0]){
			day = weatherItem.dt_txt.split(" ")[0];
			dayForecast = openWeatherWrap.forecast[day];
			if(dayForecast && dayForecast.hasOwnProperty("daily")){
				dayForecastHtml = generateWeatherDayElements(dayForecast.daily);
			} else {
				dayForecastHtml = "";
			}
			$("#full-weather-forecast").append(
				"<h3>"+
				moment(weatherItem.dt_txt.split(" ")[0]).format("dddd DD [de] MMMM")+
				"</h3>"+dayForecastHtml
			);
		}
		$("#full-weather-forecast").append(
			"<div>"+
			timeStr+": "+
			weatherItem.weather[0].description+
			", st: "+Math.round(weatherItem.main.feels_like)+"°"+
			"</div>"
		);
	}
}

function generateWeatherDayElements(forecastItem, interactive){
	return "<span class=\"wi-separation\"> </span><i class='wi weather_button "+forecastItem.icon+"'"+
	" title='"+forecastItem.descriptionLang+"'></i>"+
	"<div class=\"week-weather"+(interactive?"\" style='display:none'":"-static \"")+">"+
	"<div class='week-weather-condition'><span style='color:red'>"+forecastItem.max+"°</span><br><span style='color:blue'>"+
	forecastItem.min+"°</span></div>"+
	"<div class='week-weather-condition'><i class='wi wi-strong-wind' style='color:#b1b1b1'></i>m/s<br>"+forecastItem.wind+"</div>"+
	(forecastItem.hasOwnProperty("rain")&&forecastItem.rain>0
		?"<div class='week-weather-condition'><i class='wi wi-raindrop' style='color:#4b91ad'></i>mm<br>"+forecastItem.rain.toFixed(2)
		:"")+"</div></div>"
}


function showFullWeekForecastPopup(e){
	e.stopPropagation();
	$("#hidden-div").append($("#popup1-container").children()); //clear the popup just in case;
    $("#popup1-container").append($("#full-weather-forecast"));
    $("#popup1 h1").text("Pronóstico de la semana");
    $("#popup1").show();
    $("#popup-blocker").show();
}

function forceRefreshForecast(){
	openWeatherWrap.refreshWeatherData(function(){
		buildFullWeekForecast();
		displayWeatherData();
	})
}

function onClickWeatherIcon(e){
	e.stopPropagation();
	if($('.week-weather').is(":visible")){
		$('.week-weather').css("display","none");
	} else {
		$('.week-weather').css("display","inline-block");
	}
}

function getLastChild(tarea){
    var children = getChildrenOfTarea(tarea);
    var lastChild = children[children.length-1];
    if(getChildrenOfTarea(lastChild).length>0){
        return getLastChild(lastChild);
    } else {
        return lastChild;
    }
}
function getTaskMark(targetTask){
	var taskText = $(targetTask).find(".task-content").text();
    if(taskText.search("-")<0){
        return null;
    } else {
	    var textArr = taskText.split("-");
	    return textArr[0];
    }
}

function googleCalendarLaunch(date, time, title, location, details){
	/*window.open("http://www.google.com/calendar/event?action=TEMPLATE&"+
	"dates="+date +"T"+time+"00/"+date+"T"+time+"00&"+
	"text="+title+"&"+
	"location="+location+"&"+
	"details="+details);*/ //--> old version, somehow it included time zone and screwed everything
    /*window.open("http://calendar.google.com/calendar/eventedit?dates="+
        date+"T"+time+"00/"+date+"T"+time+"00&text="+title+
        "&location="+location+"&details="+details+
        "&sf=true&output=xml");*/
    window.open("http://calendar.google.com/calendar/gp?pli=1#~calendar:view=e&bm=1&action=TEMPLATE&text="+
        title+"&dates="+date+"T"+time+"00/"+date+"T"+time+"00&ctz=America/Argentina/Buenos_Aires&details="+
        details+"&location="+location+"&pli=1&uid&sf=true&output=xml#eventpage_6");
}

/**
*	Revisa la integridad de la lista verificando que el índice coincida con el contenido
*	Necesita el manualIndex y el dynamicIndex generado por el código inicial
*
*/
function checkListIntegrity(manualIndex,dynamicIndex){
	var completeIndex = true;
	var manualIndexHasMatch = false;
	try{
		if(!manualIndex){
			if(dynamicIndex && dynamicIndex.length==0) $("#dynamic-index-show").hide(); //no index, hide button
			return true; //no index to check integrity
		}
		for(var iManualIndex = 0; iManualIndex < manualIndex.length; iManualIndex++){
			var indexText = $(".tarea[line="+manualIndex[iManualIndex]+"]").find(".task-content").text();
			if(indexText != "¶" && indexText != "¶¶ INDICE" && indexText != "¶¶¶¶¶¶¶¶¶¶¶¶"){	// Omitimos titulo y cierre del índice, solo nos interesan las secciones que existen
				manualIndexHasMatch = false;
				dynamicIndex.forEach(function(diValue){
					if(indexText == $(diValue).find(".task-content").text()){
						manualIndexHasMatch = true;
					}
				});
				if(!manualIndexHasMatch){
					completeIndex = false;
					$("#integrity-warning").attr("failed-index",indexText);
					$("#integrity-warning").unbind();
					$("#integrity-warning").click(function(e){
						alert("El índice que falló el chequeo fue: \""+$("#integrity-warning").attr("failed-index")+"\"");
					})
					break;
				}
			}
		}
		return completeIndex;
	} catch (e){
		return false;
	}
}
/** revisa la fecha de ultima modificacion del archivo para saber si se actualizó externamente */
function checkModify(e){
		if(openedTime===0 || document.hidden) return;
		var filePath = Utils.window.getURLParameter("filename");
		var postvars = {
            action: "checkmodify",
            name:filePath
        }
        $.post("filemanager.php",postvars,function(response){
            if(response.hasOwnProperty("status") && response.status == "ok"){
                var modifiedTime = response.modified;
                if(openedTime<modifiedTime){
                	$("#modified-warning").slideDown();
                }
            }
        },"json");
}



