/**
 * Created by haswell on 22/01/17.
 */

const FILEEDIT_FILE_LOADED = "FILE_LOADED";
const FILEEDIT_FILE_SAVED = "FILE_SAVED";

//PROD agarra el archivo de las url variables
var urlVarsParse = window.location.toString().split("?")[1].split("&");
var urlVars = {};
var varPair;
var afterMessageTimeout = null;
for(var iUrlVars in urlVarsParse){
    varPair = urlVarsParse[iUrlVars].split("=");
    urlVars[varPair[0]] = varPair[1];
}

var filename = urlVars.filename;

//DEBUG agarra el archivo hardcodeado (example.txt)
//var filename = "example.txt";

var file_date = null;
var LOCALSTORAGE_FILE_BODY = "txteditable_local_file_body_" + filename;
var LOCALSTORAGE_FILE_DATE = "txteditable_local_file_date_" + filename;
var textChanged = false;
var autosaveInterval;
function autosaveDraft() {
    /** save locally first just in case */
    if (!textChanged) return;
    var now = new Date();
    var postvars = {
        name: filename,
        body: $("textarea#file-edit").val(),
        date: JSON.stringify(now)
    }
    localStorage.setItem(LOCALSTORAGE_FILE_BODY, postvars.body);
    localStorage.setItem(LOCALSTORAGE_FILE_DATE, postvars.date);
    $("#message").html("file locally auto-saved (localStorage) " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds());
    afterMessage()
    textChanged = false;
}
function savefile() {
    $("#message").html("...");
    afterMessage()
    if ($("textarea#file-edit").val().length == 0) {
        $("#message").html("cannot save an empty body!");
        afterMessage()
        return;
    }
    var now = new Date();
    var postvars = {
        action: "save",
        name: filename,
        body: $("textarea#file-edit").val(),
        date: JSON.stringify(now)
    }

    /** save locally first just in case */
    localStorage.setItem(LOCALSTORAGE_FILE_BODY, postvars.body);
    localStorage.setItem(LOCALSTORAGE_FILE_DATE, postvars.date);

    $.post("filemanager.php", postvars, function (response) {
        if (response.hasOwnProperty("status") && response.status == "ok") {
            var dateNow = new Date();
            var dateNowH = dateNow.getHours();
            var dateNowM = dateNow.getMinutes();
            var dateNowS = dateNow.getSeconds();
            if(dateNowH<10) dateNowH = "0"+dateNowH;
            if(dateNowM<10) dateNowM = "0"+dateNowM;
            if(dateNowS<10) dateNowS = "0"+dateNowS;
            $("#message").html("'"+filename+"' saved on "+dateNowH+":"+dateNowM+":"+dateNowS);
            afterMessage()
            localStorage.setItem(LOCALSTORAGE_FILE_DATE, "");
            /** clears the local data (flag) */
            textChanged = false;
            $(document).trigger(FILEEDIT_FILE_SAVED,{modified:response.modified});
        } else {
            $("#message").html("couldn't save the file, try again (saved in localStorage for now)");
            afterMessage()
        }
    }, "json")
        .error(function (e) {
            $("#message").html("couldn't save the file, try again (saved in localStorage for now)");
            afterMessage()
        });


}
$(document).ready(function () {
    $("textarea#file-edit").bind('input propertychange', function () {
        textChanged = true;
    });
    /** no es necesario porque hice que el tab anduviera de una */
    /*$("#tabBtn").on('click', function(){
     var cursorPos = $('textarea').prop('selectionStart');
     var v = $('textarea').val();
     var textBefore = v.substring(0,  cursorPos );
     var textAfter  = v.substring( cursorPos, v.length );
     $('textarea').val( textBefore+ "	" +textAfter );
     });*/
    $(document).delegate("textarea#file-edit", 'keydown', function (e) {
        var keyCode = e.keyCode || e.which;

        if (keyCode == 9) {
            e.preventDefault();
            var start = $(this).get(0).selectionStart;
            var end = $(this).get(0).selectionEnd;

            // set textarea value to: text before caret + tab + text after caret
            $(this).val($(this).val().substring(0, start)
                + "\t"
                + $(this).val().substring(end));

            // put caret at right position again
            $(this).get(0).selectionStart =
                $(this).get(0).selectionEnd = start + 1;
        }
    });

    var savedFileDate = localStorage.getItem(LOCALSTORAGE_FILE_DATE);
    if (savedFileDate && savedFileDate != "") {
        savedFileDate = JSON.parse(savedFileDate);
        $("textarea#file-edit").val(localStorage.getItem(LOCALSTORAGE_FILE_BODY));
        $("#message").html("file loaded from localStorage (couldn't save the file last time)");
        afterMessage()
        $(document).trigger(FILEEDIT_FILE_LOADED);
    } else {
        $.post(filename, function (data) { //DONT use GET or else you'll get a cached version of the file and not the actual one
            /*if(data.hasOwnProperty("status") && data.status == "ok"){
             var isNewer = true;
             var serverFileDate = JSON.parse(data.date);
             if(serverFileDate.getYear() <= savedFileDate.getYear()){
             if(serverFileDate.getMonth() >
             }
             if(isNewer){
             $("textarea").val(data.body);
             }
             }*/
            $("textarea#file-edit").val(data);
            $("#message").html("'"+filename+"' loaded from server");
            document.title = (filename.substr(filename.lastIndexOf("/")+1))+" - ListViewer"+(window.location.toString().search("workspace/listviewer_desa/")!=-1?" (desa)":"");
            afterMessage()
            $(document).trigger(FILEEDIT_FILE_LOADED);
        });
    }
    autosaveInterval = setInterval(autosaveDraft, 15000)
});
function afterMessage(){
    $("#message").slideDown();
    clearTimeout(afterMessageTimeout);
    afterMessageTimeout = setTimeout(function(){
        $("#message").slideUp();
    },6000);
}
function topBarMessage(text){
	$("#message").html(text);
	afterMessage();
}

/*$(window).unload(function(){
 if(textChanged){
 var result = confirm("Are you sure you want to reload? You have unsaved changes");
 if(result){
 window.reload();
 }
 }
 });*/
