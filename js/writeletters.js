var letters = {
a:[
"░░█░",
"░█░█",
"░███",
"░█░█",
"░█░█"
],
b:[
"░██░",
"░█░█",
"░██░",
"░█░█",
"░██░"
],
c:[
"░░██",
"░█░░",
"░█░░",
"░█░░",
"░░██"
],
d:[
"░██░",
"░█░█",
"░█░█",
"░█░█",
"░██░"
],
e:[
"░███",
"░█░░",
"░██░",
"░█░░",
"░███"
],
f:[
"░███",
"░█░░",
"░██░",
"░█░░",
"░█░░"
],
g:[
"░░██",
"░█░░",
"░█░█",
"░█░█",
"░░██"
],
h:[
"░█░█",
"░█░█",
"░███",
"░█░█",
"░█░█"
],
i:[
"░█",
"░░",
"░█",
"░█",
"░█"
],
j:[
"░░░█",
"░░░█",
"░░░█",
"░█░█",
"░░██"
],
k:[
"░█░░█",
"░█░█░",
"░██░░",
"░█░█░",
"░█░░█"
],
l:[
"░█░░",
"░█░░",
"░█░░",
"░█░░",
"░███"
],
m:[
"░█░░░█",
"░██░██",
"░█░█░█",
"░█░░░█",
"░█░░░█"
],
n:[
"░█░░░█",
"░██░░█",
"░█░█░█",
"░█░░██",
"░█░░░█"
],
enie:[
"░▀▀▄▀▀",
"░██░░█",
"░█░█░█",
"░█░░██",
"░█░░░█"
],
o:[
"░░██░",
"░█░░█",
"░█░░█",
"░█░░█",
"░░██░"
],
p:[
"░███░",
"░█░░█",
"░███░",
"░█░░░",
"░█░░░"
],
q:[
"░░██░",
"░█░░█",
"░█░░█",
"░█░██",
"░░███"
],
r:[
"░██░",
"░█░█",
"░█░█",
"░██░",
"░█░█"
],
s:[
"░░░██",
"░░█░░",
"░░░█░",
"░█░░█",
"░░██░"
],
t:[
"░███",
"░░█░",
"░░█░",
"░░█░",
"░░█░"
],
u:[
"░█░░█",
"░█░░█",
"░█░░█",
"░█░░█",
"░░██░"
],
v:[
"░█░░░█",
"░█░░░█",
"░█░░░█",
"░░█░█░",
"░░░█░░"
],
w:[
"░█░░░█",
"░█░░░█",
"░█░█░█",
"░█░█░█",
"░░█░█░"
],
x:[
"░█░░░█",
"░░█░█░",
"░░░█░░",
"░░█░█░",
"░█░░░█"
],
y:[
"░█░░░█",
"░░█░█░",
"░░░█░░",
"░░░█░░",
"░░░█░░"
],
z:[
"░███",
"░░░█",
"░░█░",
"░█░░",
"░███"
],
n1:[
"░░█░",
"░██░",
"░░█░",
"░░█░",
"░███"
],
n2:[
"░░██░",
"░█░░█",
"░░░█░",
"░░█░░",
"░████"
],
n3:[
"░██░",
"░░░█",
"░░█░",
"░░░█",
"░██░"
],
n4:[
"░█░█",
"░█░█",
"░███",
"░░░█",
"░░░█"
],
n5:[
"░███",
"░█░░",
"░░█░",
"░░░█",
"░██░"
],
n6:[
"░░██",
"░█░░",
"░███",
"░█░█",
"░███"
],
n7:[
"░███",
"░░░█",
"░░█░",
"░░█░",
"░░█░"
],
n8:[
"░███",
"░█░█",
"░███",
"░█░█",
"░███"
],
n9:[
"░███",
"░█░█",
"░███",
"░░░█",
"░███"
],
space:[
"░░░░",
"░░░░",
"░░░░",
"░░░░",
"░░░░"
],
udi:[
"░▀░░▀",
"░█░░█",
"░█░░█",
"░█░░█",
"░░██░"
],
gm:[
"░░░░",
"░░░░",
"░░██",
"░░░░",
"░░░░"
],
gb:[
"░░░░",
"░░░░",
"░░░░",
"░░░░",
"░███"
],
TEMPLATE:[
"░░░░",
"░░░░",
"░░░░",
"░░░░",
"░░░░"
],
}
var conversions = [
{find:" ",replace:"space"},
{find:"1",replace:"n1"},
{find:"2",replace:"n2"},
{find:"3",replace:"n3"},
{find:"4",replace:"n4"},
{find:"5",replace:"n5"},
{find:"6",replace:"n6"},
{find:"7",replace:"n7"},
{find:"8",replace:"n8"},
{find:"9",replace:"n9"},
{find:"á",replace:"a"},
{find:"é",replace:"e"},
{find:"í",replace:"i"},
{find:"ó",replace:"o"},
{find:"ú",replace:"u"},
{find:"ü",replace:"udi"},
{find:"ñ",replace:"enie"},
{find:"-",replace:"gm"},
{find:"_",replace:"gb"}
]
function formWordArray(word){
	var formedWord = [];
	var indexes = word.split("");
	for(var i in indexes){
		for(var iConv in conversions){
			if(conversions[iConv].find==indexes[i]){
				indexes[i] = conversions[iConv].replace;
			}
		}
		formedWord.push(letters[indexes[i]]);
	}
	return formedWord;
}
function writeWord(word){
	var wordArr = formWordArray(word.toLowerCase());
	document.getElementById("written-word").innerHTML = "";
	var printedWord = [];
	var newLine = "";
	for(var i=0;i<5;i++){
		newLine = "";
		for(var j in wordArr){
			console.log(j)
			if(!wordArr[j]) continue;
			newLine += wordArr[j][i]
		}
		printedWord.push(newLine);
	}
	printWord(printedWord);
}
function printWord(printedWord){
	console.log(printedWord[0]);
	console.log(printedWord[1]);
	console.log(printedWord[2]);
	console.log(printedWord[3]);
	console.log(printedWord[4]);
	document.getElementById("written-word").innerHTML+=printedWord[0]+"<br>";
	document.getElementById("written-word").innerHTML+=printedWord[1]+"<br>";
	document.getElementById("written-word").innerHTML+=printedWord[2]+"<br>";
	document.getElementById("written-word").innerHTML+=printedWord[3]+"<br>";
	document.getElementById("written-word").innerHTML+=printedWord[4]+"<br>";
}