var exercise = {
	minimumToPick:0,
	maximumToPick:0,
	availableRoutineItems:0,
	nonSkippableItems:0,
	selectedItems:0,
	currentItemLine:0,
	exerciseGroupTitle:null,
	routineRunning:false
};

exercise.init = function(){
	exercise.maximumToPick = matlistData.config.maxItems;
	exercise.minimumToPick = matlistData.config.minItems;
	$(".tarea").each(function(i,e){
		if($(e).hasClass("tarea-parent") && getTaskMark($(e))!="x"){ //expand all lines
			parentShowChildren(null,$(e));
		} else if(getTaskMark($(e))=="x"){	//hide items marked with x 
			$(e).hide();
		}
		//count
		if(getTaskMark($(e))=="!"){
			exercise.nonSkippableItems++;
		} else if (getTaskMark($(e))==""){
			exercise.availableRoutineItems++;
		}
		if(	$(e).find(".task-content").text().substr(0,3)=="¶¶ " && 
			$(e).find(".task-content").text().substr(3)==matlistData.config.exerciseTitle)
		{
			exercise.exerciseGroupTitle = $(e);
		}
	});
	

	$("#general-tools").append("<div id=\"exercise-tools\"></div>");
	$("#exercise-tools").append("Mínimo:<span id=\"ex-minimum-items\">"+exercise.minimumToPick+"</span> ");
	$("#exercise-tools").append("Máximo:<span id=\"ex-maximum-items\">"+exercise.maximumToPick+"</span> ");
	$("#exercise-tools").append("Items:<span id=\"ex-available-items\">"+exercise.availableRoutineItems+"</span> ");
	$("#exercise-tools").append("Seleccionados:<span id=\"ex-selected-items\">"+0+"</span> ");
	$("#exercise-tools").append("<button id=\"make-routine\">Crear rutina</button>");
	$("#make-routine").click(exercise.makeRoutine);
	$("#next-exercise").hide();
	
	$(document).on("MATLIST_ITEM_CHECKED", function (event,data) {
		let line = parseInt($(data.task).attr("line"));
		let exerciseGroupTitleLine = parseInt(exercise.exerciseGroupTitle.attr("line"));
		if(line>exerciseGroupTitleLine){
			exercise.selectedItems = $(".checked").not(":hidden").length;
			$("#ex-selected-items").html(exercise.selectedItems);
		}
	});
	
	$(document).on("MATLIST_ITEM_UNCHECK_ALL", function (event) {
		exercise.selectedItems = 0;
		$("#ex-selected-items").html(exercise.selectedItems);
	});
	
	$(document).on("MATLIST_ITEM_CLICKED", function (event,data) {
		exercise.currentItemLine = parseInt($(data.task).attr("line"));
	});

}

exercise.makeRoutine = function(){
	if(exercise.selectedItems <= exercise.maximumToPick && exercise.selectedItems >= exercise.minimumToPick){
		$(".tarea").each(function(i,e){
			if(
				!$(e).hasClass("checked") && 
				(!getTaskMark($(e)) || getTaskMark($(e)).indexOf("!")==-1) //hide if task mark is null or doesn't have important flag
			){
				$(e).hide();
			}
		});
		exercise.currentItemLine = parseInt(exercise.exerciseGroupTitle.attr("line"));
		btUnmark();
		$("#exercise-tools").html("");
		$("#exercise-tools").append("Rutina en curso ");
		$("#exercise-tools").append("<button id=\"next-exercise\">Siguiente</button>");
		$("#exercise-tools").append("<button id=\"stop-exercise\">Parar</button>");
		$("#make-routine").hide();
		$("#stop-exercise").hide();
		$("#next-exercise").show();
		$("#next-exercise").unbind();
		$("#next-exercise").click(exercise.nextItem);
		$("#stop-exercise").unbind();
		$("#stop-exercise").click(exercise.pauseRoutine);
	} else {
		alert("Debes seleccionar entre "+exercise.minimumToPick+" y "+exercise.maximumToPick+" items");
	}
	
}
exercise.nextItem = function(){
	exercise.routineRunning = true;
	$("#stop-exercise").show();
	exercise.currentItemLine++;
	let currentItem = $("[line="+exercise.currentItemLine+"]");
	if(
		//parseInt($(currentItem).attr("tab"))>0 && //ommit if it's a category
		$(currentItem).is(":visible")
	){ 
		$(currentItem).click();
		exercise.speak($(currentItem).find(".task-content").text().split("-")[1]);
		let taskMark = $(currentItem).find(".task-content").text().split("-")[0];
		let taskTimeIndex = taskMark.search(/\d\d:\d\d/g);
		if(taskTimeIndex!=-1){
			taskTime = taskMark.substr(taskTimeIndex,5);
			taskTimeArr = taskTime.split(":");
			taskTimeInSeconds = parseInt(taskTimeArr[0])*60+parseInt(taskTimeArr[1]);
			setTimeout(function(){
				if(exercise.routineRunning) exercise.nextItem();
			}, taskTimeInSeconds*1000);
		} else {
			setTimeout(function(){
				exercise.speak("pausado");
				topBarMessage("pausado");
				exercise.pauseRoutine();
			},500);
		}
	} else {
		if($("[line="+(exercise.currentItemLine+1)+"]")[0] != null){
			exercise.nextItem();	
		} else {
			exercise.speak("Fin de ejercicio");
			topBarMessage("Fin de ejercicio");
		}
	}
}

exercise.pauseRoutine = function(){
	exercise.routineRunning = false;
	$("#stop-exercise").hide();
}

exercise.speak = function(text){
	window.speechSynthesis.speak(new SpeechSynthesisUtterance(text));
}
