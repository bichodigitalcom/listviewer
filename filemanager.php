<?php
header("Access-Control-Allow-Origin: *");
error_reporting (E_ERROR);
$params_errors = array();
if(isset($_REQUEST["action"])) $action = $_REQUEST['action']; else $params_errors[] = "parameter action is missing";
$filebody = $_REQUEST["body"];
$filename = $_REQUEST["name"];

$response = new stdClass();

if(count($params_errors)>0){
    $response->errors = $params_errors;
    $response->status = "error";
} else {
    $file = null;
    if($action == "save"){
        if(file_exists($filename) && !empty($filebody)){
            date_default_timezone_set("America/Argentina/Buenos_Aires");
            if(strpos($filename,"/")){
				$filenameNoPathArr = explode("/",$filename);
				$filenameNoPath = $filenameNoPathArr[1];
			} else {
				$filenameNoPath = $filename;
			}
            $backupname = "backups/".substr($filenameNoPath,0,strlen($filenameNoPath)-4)."_backup_".date("y-m-d_H-i-s",time()).".txt";
            copy($filename,$backupname);

            $file = fopen($filename, "w+");
            $result = fwrite($file, $filebody);
            fclose($file);
            clearstatcache(); //requerido para que filemtime no te devuelva el date cacheado
            if($result!=false){
                $response->status = "ok";
                $response->modified = filemtime($filename);
            } else {
                $response->status = "error";
            }
        }
    } else if($action == "filelist"){
        $files = scandir("./files");
        $txtFiles = array();
        foreach($files as $file){
            if(substr($file,-4)==".txt"){
                $txtFiles[] = "files/".$file;
            }
        }
        $response->files = $txtFiles;
        $response->status = "ok";
    } else if($action == "checkmodify"){
    	if(file_exists($filename)){
            date_default_timezone_set("America/Argentina/Buenos_Aires");
            $result = filemtime($filename);
            if($result!=false){
                $response->status = "ok";
                $response->modified = $result;
            } else {
                $response->status = "error";
                $response->message = "Failed getting file modification time";
            }
        } else {
        	$response->status = "error";
        	$response->message = "file does no exists";
        }
    }
}
echo json_encode($response);