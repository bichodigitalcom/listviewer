<?php
header("Access-Control-Allow-Origin: *");
error_reporting (E_ERROR);
if(isset($_REQUEST["action"])) $action = $_REQUEST['action']; else $params_errors[] = "parameter action is missing";
$filebody = $_REQUEST["body"];
$filename = $_REQUEST["name"];

$response = new stdClass();

if(count($params_errors)>0){
    $response->errors = $params_errors;
    $response->status = "error";
} else {
    $file = null;
    if($action == "save"){
        if(file_exists($filename) && !empty($filebody)){
            date_default_timezone_set("America/Argentina/Buenos_Aires");
            $backupname = "../backups/".substr($filename,0,strlen($filename)-4)."_backup_".date("y-m-d_H-i-s",time()).".txt";
            copy($filename,$backupname);

            $file = fopen($filename, "w+");
            $result = fwrite($file, $filebody);
            fclose($file);
            if($result!=false){
                $response->status = "ok";
                $response->body = $filebody;
            } else {
                $response->status = "error";
            }
        }
    } else if($action == "filelist"){
        $files = scandir(".");
        $txtFiles = array();
        foreach($files as $file){
            if(substr($file,-4)==".txt"){
                $txtFiles[] = $file;
            }
        }
        $response->files = $txtFiles;
        $response->status = "ok";
    }
}
echo json_encode($response);