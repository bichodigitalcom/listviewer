Los archivos actualmente de listviewer (implementación mat) son:
	-backup/ (vacía, ahí se generan archivos de backup automaticamente)
	-css/ (todo)
	-js/ (todo)
	-data/worklog.json
	-data/filemanager.php
	-index.html
	-filemanager.php
	-LISTVIEWER-README.txt

Los índices se hacen a manopla, pero una vez hechos se chequea que todo lo que figura en el íncide esté en el documento, y sinó tira una alerta de que el documento quizás está incompleto o corrupto (ha pasado que al salvar ocurra un bug y te salve media lista).



Formato del índice:
	-titulo del documento
	-espacio
	-titulo armado con caracteres gráficos para que se lea en más grande (utilizar tool "WriteLetters" que hice para esto).
	-espacio
	-espacio
	-indice (ver ejemplo de formato abajo)
	-espacio
	-espacio
	-empieza el documento

Ejemplo de índice manual (o del comienzo del documento):

---(inicio del ejemplo)---
NOMBRE DEL DOCUMENTO

░▓░░░▓░░▓▓░░▓░░░▓░▓▓░░▓▓░░▓▓▓░░░░░▓▓░░▓▓▓░▓░░░░░░░▓▓░░░▓▓░░░▓▓░▓░░▓░▓░░░▓░▓▓▓░▓░░░▓░▓▓▓░░▓▓░
░▓▓░░▓░▓░░▓░▓▓░▓▓░▓░▓░▓░▓░▓░░░░░░░▓░▓░▓░░░▓░░░░░░░▓░▓░▓░░▓░▓░░░▓░░▓░▓▓░▓▓░▓░░░▓▓░░▓░░▓░░▓░░▓
░▓░▓░▓░▓░░▓░▓░▓░▓░▓▓░░▓░▓░▓▓░░░░░░▓░▓░▓▓░░▓░░░░░░░▓░▓░▓░░▓░▓░░░▓░░▓░▓░▓░▓░▓▓░░▓░▓░▓░░▓░░▓░░▓
░▓░░▓▓░▓░░▓░▓░░░▓░▓░▓░▓▓░░▓░░░░░░░▓░▓░▓░░░▓░░░░░░░▓░▓░▓░░▓░▓░░░▓░░▓░▓░░░▓░▓░░░▓░░▓▓░░▓░░▓░░▓
░▓░░░▓░░▓▓░░▓░░░▓░▓▓░░▓░▓░▓▓▓░░░░░▓▓░░▓▓▓░▓▓▓░░░░░▓▓░░░▓▓░░░▓▓░░▓▓░░▓░░░▓░▓▓▓░▓░░░▓░░▓░░░▓▓░


¶¶ INDICE
¶
¶¶ CATEGORÍA PRINCIPAL 1
¶¶ CATEGORÍA PRINCIPAL 2
¶¶ CATEGORÍA PRINCIPAL 3
¶ CATEGORÍA MENOR O MAS ESPECIFICA 1
¶ CATEGORÍA MENOR O MAS ESPECIFICA 2
¶ CATEGORÍA MENOR O MAS ESPECIFICA 3
¶¶ CATEGORÍA PRINCIPAL 4
¶ CATEGORÍA MENOR O MAS ESPECIFICA 4A
¶ CATEGORÍA MENOR O MAS ESPECIFICA 4B
¶¶¶¶¶¶¶¶¶¶¶¶


¶¶ CONFIG (aca es donde empieza el documento)
x-hacer esto
-hacer lo otro
-etcétera
---(fin del ejemplo)---
